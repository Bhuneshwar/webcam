# VIN App

## Change API base url:
apiUrl : src/environments/environment.prod.ts

## Start the server
Production Server : (For Deployment)

## FRONT:
$ npm run startProd 
$ NODE_PORT=8080 pm2 start dist/server.js  (for pm2)
$ PORT=8080 node dist/server.js (for local system)

## WS :
$ NODE_PORT=5001 pm2 start WS/server.js  (for pm2)
$ PORT=5001 node WS/server.js (for local system)
