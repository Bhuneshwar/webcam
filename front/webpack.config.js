const path = require('path');
const nodeExternals = require('webpack-node-externals');
const webpack = require('webpack');

module.exports = {
  entry: {
    server: './src/server.ts'
  },
  resolve: {
    extensions: ['.ts', '.js']
  },
  target: 'node',
   externals: [nodeExternals({
     whitelist: [
      /^@angular\/material/,
      /^@ng-bootstrap\/ng-bootstrap/,
      /^angular2-toaster/,
      /^ngx-cookie-service/
     ]
   })],
  node: {
    __dirname: true
  },
  output: {
    path: path.join(__dirname, 'dist'),
    filename: '[name].js'
  },
  module: {
    rules: [
      { test: /\.ts$/, loader: 'ts-loader' }
    ]
  },
  plugins: [
    new webpack.DefinePlugin({
        location: JSON.stringify({
            protocol: 'https',
            host: `localhost`,
        })
    })
  ]
}
