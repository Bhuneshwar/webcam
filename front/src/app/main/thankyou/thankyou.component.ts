import { Component, OnInit } from '@angular/core';
import { trigger , state , style , animate , transition, keyframes } from '@angular/animations';
import { Injector, NgZone } from '@angular/core'; 
import { BaseComponent } from '../../common/commonComponent';
@Component({
  selector: 'app-thankyou',
  templateUrl: './thankyou.component.html'
})
export class ThankyouComponent extends BaseComponent implements OnInit {

  constructor(injector: Injector) {
    super(injector)
  }

  ngOnInit() {
    //this.popToast('success','Welcome','Successfully logged in.')
  }

}
