import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { HomePageComponent } from './home-page/home-page.component';
import { MainComponent } from './main/main.component';
import { DetailComponent } from './detail/detail.component';
import { ThankyouComponent } from './thankyou/thankyou.component';
import { AlwaysAuthChildrenGuard, CanLoginActivate } from './../common/auth.gaurd';
import { CameraComponent } from './camera/camera.component';
import { CameraService } from './camera/camera.service';
@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild([
      { path: '', component: MainComponent, children : [
          { path: '', redirectTo: 'home', pathMatch: 'full'},
          { path: 'home', component: HomePageComponent, pathMatch: 'full'},
          { path: 'detail', component: DetailComponent, pathMatch: 'full'},
           { path: 'thankyou', component: ThankyouComponent, pathMatch: 'full'},
          { path: 'camera', component: CameraComponent, pathMatch: 'full'},
          { path: '**', redirectTo: 'home', pathMatch: 'full' }
        ]
      },
      { path: '**', redirectTo: '', pathMatch: 'full' }
    ])
  ],
  providers : [CameraService],
  declarations: [HomePageComponent, DetailComponent, MainComponent, CameraComponent, ThankyouComponent]
})
export class MainModule { }

