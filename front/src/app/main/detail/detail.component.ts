import { Component, OnInit } from '@angular/core';
import { trigger , state , style , animate , transition, keyframes } from '@angular/animations';
import { Injector, NgZone } from '@angular/core'; 
import { BaseComponent } from '../../common/commonComponent';
@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html'
})
export class DetailComponent extends BaseComponent implements OnInit {

  constructor(injector: Injector) {
    super(injector)
  }

  public discData;

  ngOnInit() {
  	this.discData = JSON.parse(this.getToken('disc_data'));
    console.log('disc Data : ', this.discData)
    //this.popToast('success','Welcome','Successfully logged in.')
  }

}
