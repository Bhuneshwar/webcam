import { Component, OnInit, AfterViewInit, ViewChild, Input, Injector } from '@angular/core';
import { CameraService } from './camera.service';
import { Subscription } from 'rxjs/Subscription';
import { BaseComponent } from '../../common/commonComponent';

@Component({
  selector: 'app-camera',
  templateUrl: './camera.component.html',
  styleUrls: ['./camera.component.css'],
  providers: [ CameraService ]
})
export class CameraComponent extends BaseComponent implements OnInit, AfterViewInit {
  @ViewChild('videoplayer') videoPlayer: any;
  @ViewChild('canvas') canvas: any;
  public showVideo: any = false;  // Fungerar fast tvärtom. Byt logik

  context: any;

  @Input() width: number;
  @Input() height: number;

  constructor(injector: Injector, private cameraService: CameraService) { 
       super(injector)
  }

  capture() {
    // this.context = this.canvas.nativeElement.getContext('2d');
    this.context.drawImage(this.videoPlayer.nativeElement, 0, 0, this.width, this.height);
    this.showVideo = true;
  }

  public myImg: any;
  saveImage() {
    this.showVideo = false;

    let imgData: any = this.canvas.nativeElement.toDataURL('img/png');
    // console.log(imgData);

    this.myImg = imgData;

    imgData = imgData.replace('data:image/png;base64,', '');

    let postData: any = JSON.stringify({
      'ImageBase64String': imgData,
      'id': 3
    });

     console.log(postData);
     this.setToken('motor_img_url', this.myImg)
     this.popToast('success','Uploaded','Image Uploaded Successfully')
     //Call api here()
  }


  ngOnInit() {
    this.width = 400;
    this.height = 300;
  }

  ngAfterViewInit() {
    this.context = this.canvas.nativeElement.getContext('2d');
    if (navigator.mediaDevices && navigator.mediaDevices.getUserMedia) {
          setTimeout(()=>{
             navigator.mediaDevices.getUserMedia({ video: true }).then(stream => {
            if("srcObject" in this.videoPlayer.nativeElement){
              this.videoPlayer.nativeElement.srcObject = stream;
            }else{
              this.videoPlayer.nativeElement.src = window.URL.createObjectURL(stream);
            }
            this.videoPlayer.nativeElement.onloadedmetadata = function(e) {
              if(this.videoPlayer && this.videoPlayer.nativeElement){
                this.videoPlayer.nativeElement.play();  
              }
            };
          }).catch(function(err) {
             alert('Error:'+ err.message);
          });
          },2000)
          
    }else{
        alert("Doesn't support devices.")
    }
  }
}
