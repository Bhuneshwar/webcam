import { Component, OnInit} from '@angular/core';
import { trigger , state , style , animate , transition, keyframes } from '@angular/animations';
import { Injector, NgZone } from '@angular/core'; 
import { BaseComponent } from '../../common/commonComponent';
import { Http, Request } from '@angular/http';
import { Router } from "@angular/router";
@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html'
})
export class HomePageComponent extends BaseComponent implements OnInit {

  constructor(injector: Injector) {
    super(injector)
  }

  ngOnInit() {
    //this.popToast('success','Welcome','Successfully logged in.')
    this.showLoader = false;
    console.log('showLoader', this.showLoader)
  }
  public showLoader :boolean;
  onFileChange(event) {
    let reader = new FileReader();
    if(event.target.files && event.target.files.length > 0) {
      let file = event.target.files[0];
      reader.readAsDataURL(file);
      reader.onload = () => {
      	console.log('file', file)
      	//Call file upload api here 
      	//onsuccess redirect to next step
        var fd = new FormData();
        fd.append('image', file)
        this.showLoader = true;
        this.commonService.uploadDisc(fd).subscribe(success => {
          this.showLoader = false;
          if(success.success == 1){
            let discData = {
              licenseNo : success[6],
              vehRegNo : success[7],
              description : success[8],
              make : success[9],
              vin : success[12],
              engineNo : success[13]
            }
            this.setToken('disc_data',JSON.stringify(discData))
            this.router.navigate(['/main/detail'])
          }else if(success.success == 0){
            this.popToast('error','Error','Please Scan the disc properly.')
          }
        },error =>{
          this.showLoader = false;
          console.log('error', error)
          this.popToast('error','Error','Please Scan the disc properly.')
        });
       	
    }
  }
  }
}