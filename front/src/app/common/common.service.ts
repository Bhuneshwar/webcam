import { BaseComponent } from './../common/commonComponent';
import { Injectable, Injector } from '@angular/core';
import { CoolLocalStorage } from 'angular2-cool-storage';
import { Http, Response,Headers,RequestOptions } from '@angular/http';

@Injectable()
export class CommonService{
 
  authorised :any= false;

  constructor(inj :Injector, public localStorage:CoolLocalStorage, private _http: Http) { 
  	//super(inj);
     this._apiUrl = 'http://10.2.1.85/vinapp/'
  }
  
  public _apiUrl : any;
  public _apiUrl1 = 'http://localhost:4021/apis/'

  authorise() {
  	if(this.localStorage.getItem("authToken")){
    	this.authorised = true;
  	}
  	else{
  		this.authorised = false;
  	}
    //console.log('common called after',this.authorised)
  }

  /****************************************************************************
  @PURPOSE      : User Login API 
  @PARAMETERS   : <user_info_obj>
  @RETURN       : <success_data>
  ****************************************************************************/
  uploadDisc(data){
    let headers = new Headers({});
    let options = new RequestOptions({headers:headers});
    //return this._http.post(this._apiUrl+'api/'+'userLogin',JSON.stringify(user),options)
    return this._http.post(this._apiUrl+'upload_license.php',data,options)
    .map((response:Response) => response.json());
  }
  /****************************************************************************/

}