import { Component, OnInit } from '@angular/core';
import { Injector, NgZone } from '@angular/core';
import { CoolLocalStorage } from 'angular2-cool-storage';
import { ToasterService} from 'angular2-toaster';
import { Router } from "@angular/router";
import { CookieService } from 'ngx-cookie-service';
import { CommonService } from './common.service';
import { environment } from '../../environments/environment';
import { Http, Response,Headers,RequestOptions } from '@angular/http';
import {Location} from '@angular/common';
@Component({
    selector: 'parent-comp',
    template: `<h1>Hello from ParentComponent </h1>`,
    providers: []
})

export class BaseComponent {
    
    constructor(injector: Injector) {    
        this.localStorage = injector.get(CoolLocalStorage) //Inject any service.
        this.toasterService = injector.get(ToasterService)
        this.router = injector.get(Router)
        this.cookie = injector.get(CookieService)
        this.commonService = injector.get(CommonService)
        this.http = injector.get(Http)
        this.router = injector.get(Router)
        //console.log('Your current Environment is :', environment)
        //this.popToast('success','test','message')
         
    }

    public showBack = true; 


    checkBack(){
        if(location.pathname == '/main/home'){
            return false;
        } 
        return true;  
    }

    goBack(){
        let stateName = this.router.url
        if(stateName == '/main/detail'){
            this.router.navigate(['/main/home'])
        }else if(stateName == '/main/thankyou'){
            this.router.navigate(['/main/detail'])
        }
    }    

    public http = this.http;
    public router : Router;
    public commonService : CommonService;
    //*************************************************************//
    //@Purpose : We can use following function to use localstorage (CoolLocalStorage).
    //*************************************************************//
    public localStorage : CoolLocalStorage;
    setToken(key,value){
        this.localStorage.setItem(key, value)
    }
    getToken(key){
        return this.localStorage.getItem(key)
    }
    removeToken(key){
        this.localStorage.removeItem(key)
    }
    clearToken(){
        this.localStorage.clear()
    }
    //*************************************************************//


    //*************************************************************//
    //@Purpose : We can use following function to use cookie storage 
    //*************************************************************//
    public cookie : CookieService;
    setCookie(key,value){
        this.cookie.set(key, value)
    }
    getCookie(key){
        this.cookie.get(key)
    }
    removeCookie(key){
        this.cookie.delete(key)
    }
    clearCookie(){
        this.cookie.deleteAll()
    }
    //*************************************************************//

    //*************************************************************//
    //@Purpose : We can use following function to use Toaster Service.
    //*************************************************************//
    public toasterService : ToasterService;
    popToast(type,title,body){
        this.toasterService.pop(type,title,body)
    }

    //*************************************************************//
    //@Purpose : To prevent any key on keydown.
    //*************************************************************//
    keyDownHandler(event, key?) {
      if(key == 'space'){
          if (event.which === 32){
              event.preventDefault();
          }
      }
    }

    logout(){
        this.clearToken()
        //this.removeCookie('authToken')
        console.log('logout called ') //call logout api here.
        this.router.navigate(["/login"]);
    }

    /****************************************************************************
      @PURPOSE      : Test api call
      @PARAMETERS   : <user_info_obj>
      @RETURN       : <success_data>
    /****************************************************************************/
      callApi(){
           // let headers = new Headers({'content-Type':'application/json'});
           // let options = new RequestOptions({headers : headers});
            return this.http.get(environment.apiUrl+'posts/1')
            .map((response:Response) => response.json());
        }
    /****************************************************************************/


}