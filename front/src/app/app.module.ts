import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, PreloadAllModules} from '@angular/router';
import { AppComponent } from './app.component';
import { FormsModule }   from '@angular/forms';
import { CoolStorageModule } from 'angular2-cool-storage';
import {ToasterModule, ToasterService} from 'angular2-toaster';
export { AppComponent };
import { BaseComponent } from './common/commonComponent';
import { AlwaysAuthChildrenGuard, CanLoginActivate } from './common/auth.gaurd';
import { CookieService } from 'ngx-cookie-service';
import { CommonService } from './common/common.service';
import { HttpModule } from '@angular/http';

@NgModule({
  declarations: [
    AppComponent,
    BaseComponent
  ],
  imports: [
    HttpModule,
    CoolStorageModule, ToasterModule,
    FormsModule,
    BrowserModule.withServerTransition({ appId: 'vin-frontend-ng4'}),
    RouterModule.forRoot([
      { path: 'main', loadChildren: './main/main.module#MainModule'},
      { path: '', redirectTo : 'main', pathMatch : 'full'},
      { path: '**', redirectTo: 'main', pathMatch: 'full'}
    ], {initialNavigation:'enabled', useHash: false})
  ],
  exports: [AppComponent],
  providers: [AlwaysAuthChildrenGuard, CanLoginActivate, CookieService, CommonService, ToasterService],
  bootstrap: [AppComponent]
})
export class AppModule { }