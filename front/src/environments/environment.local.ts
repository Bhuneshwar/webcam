export const environment = {
  env : 'local',
  production: true,
  apiUrl : 'http://localhost:5001/', //use local url here.
  port : 5000
};
