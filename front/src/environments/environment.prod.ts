export const environment = {
  env: 'prod',
  production: true,
  apiUrl : 'https://jsonplaceholder.typicode.com/', //Please use live (eg. aws) url here. after deployment.
  port : 8080
};
