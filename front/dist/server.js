/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// identity function for calling harmony imports with the correct context
/******/ 	__webpack_require__.i = function(value) { return value; };
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 65);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports) {

module.exports = require("@angular/core");

/***/ }),
/* 1 */
/***/ (function(module, exports) {

module.exports = require("@angular/router");

/***/ }),
/* 2 */
/***/ (function(module, exports) {

module.exports = require("@angular/common");

/***/ }),
/* 3 */
/***/ (function(module, exports) {

module.exports = require("@angular/platform-browser");

/***/ }),
/* 4 */
/***/ (function(module, exports) {

module.exports = require("@angular/http");

/***/ }),
/* 5 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__angular_core__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_Observable__ = __webpack_require__(41);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_Observable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_rxjs_Observable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_share__ = __webpack_require__(64);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_share___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_share__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_Subject__ = __webpack_require__(61);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_Subject___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_Subject__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ToasterService", function() { return ToasterService; });




var ToasterService = (function () {
    /**
     * Creates an instance of ToasterService.
     */
    function ToasterService() {
        var _this = this;
        this.addToast = new __WEBPACK_IMPORTED_MODULE_1_rxjs_Observable__["Observable"](function (observer) { return _this._addToast = observer; }).share();
        this.clearToasts = new __WEBPACK_IMPORTED_MODULE_1_rxjs_Observable__["Observable"](function (observer) { return _this._clearToasts = observer; }).share();
        this._removeToastSubject = new __WEBPACK_IMPORTED_MODULE_3_rxjs_Subject__["Subject"]();
        this.removeToast = this._removeToastSubject.share();
    }
    /**
     * Synchronously create and show a new toast instance.
     *
     * @param {(string | Toast)} type The type of the toast, or a Toast object.
     * @param {string=} title The toast title.
     * @param {string=} body The toast body.
     * @returns {Toast}
     *          The newly created Toast instance with a randomly generated GUID Id.
     */
    ToasterService.prototype.pop = function (type, title, body) {
        var toast = typeof type === 'string' ? { type: type, title: title, body: body } : type;
        toast.toastId = Guid.newGuid();
        if (!this._addToast) {
            throw new Error('No Toaster Containers have been initialized to receive toasts.');
        }
        this._addToast.next(toast);
        return toast;
    };
    /**
     * Asynchronously create and show a new toast instance.
     *
     * @param {(string | Toast)} type The type of the toast, or a Toast object.
     * @param {string=} title The toast title.
     * @param {string=} body The toast body.
     * @returns {Observable<Toast>}
     *          A hot Observable that can be subscribed to in order to receive the Toast instance
     *          with a randomly generated GUID Id.
     */
    ToasterService.prototype.popAsync = function (type, title, body) {
        var _this = this;
        setTimeout(function () {
            _this.pop(type, title, body);
        }, 0);
        return this.addToast;
    };
    /**
     * Clears a toast by toastId and/or toastContainerId.
     *
     * @param {string} toastId The toastId to clear.
     * @param {number=} toastContainerId
     *        The toastContainerId of the container to remove toasts from.
     */
    ToasterService.prototype.clear = function (toastId, toastContainerId) {
        var clearWrapper = {
            toastId: toastId, toastContainerId: toastContainerId
        };
        this._clearToasts.next(clearWrapper);
    };
    return ToasterService;
}());

ToasterService.decorators = [
    { type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"] },
];
/** @nocollapse */
ToasterService.ctorParameters = function () { return []; };
// http://stackoverflow.com/questions/26501688/a-typescript-guid-class
var Guid = (function () {
    function Guid() {
    }
    Guid.newGuid = function () {
        return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
            var r = Math.random() * 16 | 0, v = c === 'x' ? r : (r & 0x3 | 0x8);
            return v.toString(16);
        });
    };
    return Guid;
}());
//# sourceMappingURL=toaster.service.js.map

/***/ }),
/* 6 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(0);
var core_2 = __webpack_require__(0);
var angular2_cool_storage_1 = __webpack_require__(19);
var angular2_toaster_1 = __webpack_require__(21);
var router_1 = __webpack_require__(1);
var ngx_cookie_service_1 = __webpack_require__(34);
var common_service_1 = __webpack_require__(9);
var environment_1 = __webpack_require__(59);
var http_1 = __webpack_require__(4);
var BaseComponent = (function () {
    function BaseComponent(injector) {
        this.showBack = true;
        this.http = this.http;
        this.localStorage = injector.get(angular2_cool_storage_1.CoolLocalStorage); //Inject any service.
        this.toasterService = injector.get(angular2_toaster_1.ToasterService);
        this.router = injector.get(router_1.Router);
        this.cookie = injector.get(ngx_cookie_service_1.CookieService);
        this.commonService = injector.get(common_service_1.CommonService);
        this.http = injector.get(http_1.Http);
        this.router = injector.get(router_1.Router);
        //console.log('Your current Environment is :', environment)
        //this.popToast('success','test','message')
    }
    BaseComponent.prototype.checkBack = function () {
        if ({"protocol":"https","host":"localhost"}.pathname == '/main/home') {
            return false;
        }
        return true;
    };
    BaseComponent.prototype.goBack = function () {
        var stateName = this.router.url;
        if (stateName == '/main/detail') {
            this.router.navigate(['/main/home']);
        }
        else if (stateName == '/main/thankyou') {
            this.router.navigate(['/main/detail']);
        }
    };
    BaseComponent.prototype.setToken = function (key, value) {
        this.localStorage.setItem(key, value);
    };
    BaseComponent.prototype.getToken = function (key) {
        return this.localStorage.getItem(key);
    };
    BaseComponent.prototype.removeToken = function (key) {
        this.localStorage.removeItem(key);
    };
    BaseComponent.prototype.clearToken = function () {
        this.localStorage.clear();
    };
    BaseComponent.prototype.setCookie = function (key, value) {
        this.cookie.set(key, value);
    };
    BaseComponent.prototype.getCookie = function (key) {
        this.cookie.get(key);
    };
    BaseComponent.prototype.removeCookie = function (key) {
        this.cookie.delete(key);
    };
    BaseComponent.prototype.clearCookie = function () {
        this.cookie.deleteAll();
    };
    BaseComponent.prototype.popToast = function (type, title, body) {
        this.toasterService.pop(type, title, body);
    };
    //*************************************************************//
    //@Purpose : To prevent any key on keydown.
    //*************************************************************//
    BaseComponent.prototype.keyDownHandler = function (event, key) {
        if (key == 'space') {
            if (event.which === 32) {
                event.preventDefault();
            }
        }
    };
    BaseComponent.prototype.logout = function () {
        this.clearToken();
        //this.removeCookie('authToken')
        console.log('logout called '); //call logout api here.
        this.router.navigate(["/login"]);
    };
    /****************************************************************************
      @PURPOSE      : Test api call
      @PARAMETERS   : <user_info_obj>
      @RETURN       : <success_data>
    /****************************************************************************/
    BaseComponent.prototype.callApi = function () {
        // let headers = new Headers({'content-Type':'application/json'});
        // let options = new RequestOptions({headers : headers});
        return this.http.get(environment_1.environment.apiUrl + 'posts/1')
            .map(function (response) { return response.json(); });
    };
    return BaseComponent;
}());
BaseComponent = __decorate([
    core_1.Component({
        selector: 'parent-comp',
        template: "<h1>Hello from ParentComponent </h1>",
        providers: []
    }),
    __metadata("design:paramtypes", [core_2.Injector])
], BaseComponent);
exports.BaseComponent = BaseComponent;


/***/ }),
/* 7 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(0);
var router_1 = __webpack_require__(1);
var platform_browser_1 = __webpack_require__(3);
var angular2_toaster_1 = __webpack_require__(21);
var AppComponent = (function () {
    function AppComponent(_router, _meta, _title) {
        this._router = _router;
        this._meta = _meta;
        this._title = _title;
        this.toasterconfig = new angular2_toaster_1.ToasterConfig({
            showCloseButton: false,
            tapToDismiss: true,
            timeout: 3000,
            limit: 1
        });
        this.isLoggedIn = false;
    }
    AppComponent.prototype.ngOnInit = function () {
        var _this = this;
        this._router.events.subscribe(function (event) {
            if (event instanceof router_1.NavigationEnd) {
                switch (event.urlAfterRedirects) {
                    case '/':
                        _this._title.setTitle('Home Page');
                        _this._meta.updateTag({ name: 'description', content: 'Home Page Description' });
                        break;
                    case '/about':
                        _this._title.setTitle('About Page');
                        _this._meta.updateTag({ name: 'description', content: 'About Page Description' });
                        break;
                }
            }
        });
    };
    return AppComponent;
}());
AppComponent = __decorate([
    core_1.Component({
        selector: 'app-root',
        templateUrl: './app.component.html',
        styleUrls: ['./app.component.css']
    }),
    __metadata("design:paramtypes", [router_1.Router, platform_browser_1.Meta, platform_browser_1.Title])
], AppComponent);
exports.AppComponent = AppComponent;


/***/ }),
/* 8 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var platform_browser_1 = __webpack_require__(3);
var core_1 = __webpack_require__(0);
var router_1 = __webpack_require__(1);
var app_component_1 = __webpack_require__(7);
exports.AppComponent = app_component_1.AppComponent;
var forms_1 = __webpack_require__(17);
var angular2_cool_storage_1 = __webpack_require__(19);
var angular2_toaster_1 = __webpack_require__(21);
var commonComponent_1 = __webpack_require__(6);
var auth_gaurd_1 = __webpack_require__(15);
var ngx_cookie_service_1 = __webpack_require__(34);
var common_service_1 = __webpack_require__(9);
var http_1 = __webpack_require__(4);
var AppModule = (function () {
    function AppModule() {
    }
    return AppModule;
}());
AppModule = __decorate([
    core_1.NgModule({
        declarations: [
            app_component_1.AppComponent,
            commonComponent_1.BaseComponent
        ],
        imports: [
            http_1.HttpModule,
            angular2_cool_storage_1.CoolStorageModule, angular2_toaster_1.ToasterModule,
            forms_1.FormsModule,
            platform_browser_1.BrowserModule.withServerTransition({ appId: 'vin-frontend-ng4' }),
            router_1.RouterModule.forRoot([
                { path: 'main', loadChildren: './main/main.module#MainModule' },
                { path: '', redirectTo: 'main', pathMatch: 'full' },
                { path: '**', redirectTo: 'main', pathMatch: 'full' }
            ], { initialNavigation: 'enabled', useHash: false })
        ],
        exports: [app_component_1.AppComponent],
        providers: [auth_gaurd_1.AlwaysAuthChildrenGuard, auth_gaurd_1.CanLoginActivate, ngx_cookie_service_1.CookieService, common_service_1.CommonService, angular2_toaster_1.ToasterService],
        bootstrap: [app_component_1.AppComponent]
    })
], AppModule);
exports.AppModule = AppModule;


/***/ }),
/* 9 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(0);
var angular2_cool_storage_1 = __webpack_require__(19);
var http_1 = __webpack_require__(4);
var CommonService = (function () {
    function CommonService(inj, localStorage, _http) {
        this.localStorage = localStorage;
        this._http = _http;
        this.authorised = false;
        this._apiUrl1 = 'http://localhost:4021/apis/';
        //super(inj);
        this._apiUrl = 'http://10.2.1.85/vinapp/';
    }
    CommonService.prototype.authorise = function () {
        if (this.localStorage.getItem("authToken")) {
            this.authorised = true;
        }
        else {
            this.authorised = false;
        }
        //console.log('common called after',this.authorised)
    };
    /****************************************************************************
    @PURPOSE      : User Login API
    @PARAMETERS   : <user_info_obj>
    @RETURN       : <success_data>
    ****************************************************************************/
    CommonService.prototype.uploadDisc = function (data) {
        var headers = new http_1.Headers({});
        var options = new http_1.RequestOptions({ headers: headers });
        //return this._http.post(this._apiUrl+'api/'+'userLogin',JSON.stringify(user),options)
        return this._http.post(this._apiUrl + 'upload_license.php', data, options)
            .map(function (response) { return response.json(); });
    };
    return CommonService;
}());
CommonService = __decorate([
    core_1.Injectable(),
    __metadata("design:paramtypes", [core_1.Injector, angular2_cool_storage_1.CoolLocalStorage, http_1.Http])
], CommonService);
exports.CommonService = CommonService;


/***/ }),
/* 10 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__angular_core__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__bodyOutputType__ = __webpack_require__(22);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ToastComponent", function() { return ToastComponent; });



var ToastComponent = (function () {
    function ToastComponent(sanitizer, componentFactoryResolver, changeDetectorRef) {
        this.sanitizer = sanitizer;
        this.componentFactoryResolver = componentFactoryResolver;
        this.changeDetectorRef = changeDetectorRef;
        this.bodyOutputType = __WEBPACK_IMPORTED_MODULE_2__bodyOutputType__["a" /* BodyOutputType */];
        this.clickEvent = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
    }
    ToastComponent.prototype.ngOnInit = function () {
        if (this.toast.closeHtml) {
            this.safeCloseHtml = this.sanitizer.bypassSecurityTrustHtml(this.toast.closeHtml);
        }
    };
    ToastComponent.prototype.ngAfterViewInit = function () {
        if (this.toast.bodyOutputType === this.bodyOutputType.Component) {
            var component = this.componentFactoryResolver.resolveComponentFactory(this.toast.body);
            var componentInstance = this.componentBody.createComponent(component, undefined, this.componentBody.injector);
            componentInstance.instance.toast = this.toast;
            this.changeDetectorRef.detectChanges();
        }
    };
    ToastComponent.prototype.click = function (event, toast) {
        event.stopPropagation();
        this.clickEvent.emit({
            value: { toast: toast, isCloseButton: true }
        });
    };
    return ToastComponent;
}());

ToastComponent.decorators = [
    { type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"], args: [{
                selector: '[toastComp]',
                template: "\n        <i class=\"toaster-icon\" [ngClass]=\"iconClass\"></i>\n        <div class=\"toast-content\">\n            <div [ngClass]=\"toast.toasterConfig?.titleClass\">{{toast.title}}</div>\n            <div [ngClass]=\"toast.toasterConfig?.messageClass\" [ngSwitch]=\"toast.bodyOutputType\">\n                <div *ngSwitchCase=\"bodyOutputType.Component\" #componentBody></div>\n                <div *ngSwitchCase=\"bodyOutputType.TrustedHtml\" [innerHTML]=\"toast.body\"></div>\n                <div *ngSwitchCase=\"bodyOutputType.Default\">{{toast.body}}</div>\n            </div>\n        </div>\n        <div class=\"toast-close-button\" *ngIf=\"toast.showCloseButton\" (click)=\"click($event, toast)\"\n            [innerHTML]=\"safeCloseHtml\">\n        </div>"
            },] },
];
/** @nocollapse */
ToastComponent.ctorParameters = function () { return [
    { type: __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__["DomSanitizer"], },
    { type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["ComponentFactoryResolver"], },
    { type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["ChangeDetectorRef"], },
]; };
ToastComponent.propDecorators = {
    'toast': [{ type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"] },],
    'iconClass': [{ type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"] },],
    'componentBody': [{ type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"], args: ['componentBody', { read: __WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewContainerRef"] },] },],
    'clickEvent': [{ type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"] },],
};
//# sourceMappingURL=toast.component.js.map

/***/ }),
/* 11 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__angular_core__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_animations__ = __webpack_require__(28);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_animations___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1__angular_animations__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__toaster_config__ = __webpack_require__(33);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__toaster_service__ = __webpack_require__(5);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ToasterContainerComponent", function() { return ToasterContainerComponent; });




var ToasterContainerComponent = (function () {
    function ToasterContainerComponent(toasterService, ref, ngZone) {
        this.ref = ref;
        this.ngZone = ngZone;
        this.toasts = [];
        this.toasterService = toasterService;
    }
    ToasterContainerComponent.prototype.ngOnInit = function () {
        this.registerSubscribers();
        if (this.toasterconfig === null || typeof this.toasterconfig === 'undefined') {
            this.toasterconfig = new __WEBPACK_IMPORTED_MODULE_2__toaster_config__["a" /* ToasterConfig */]();
        }
    };
    // event handlers
    ToasterContainerComponent.prototype.click = function (toast, isCloseButton) {
        if (this.toasterconfig.tapToDismiss || (toast.showCloseButton && isCloseButton)) {
            var removeToast = true;
            if (toast.clickHandler) {
                if (typeof toast.clickHandler === 'function') {
                    removeToast = toast.clickHandler(toast, isCloseButton);
                }
                else {
                    console.log('The toast click handler is not a callable function.');
                    return false;
                }
            }
            if (removeToast) {
                this.removeToast(toast);
            }
        }
    };
    ToasterContainerComponent.prototype.childClick = function ($event) {
        this.click($event.value.toast, $event.value.isCloseButton);
    };
    ToasterContainerComponent.prototype.stopTimer = function (toast) {
        if (this.toasterconfig.mouseoverTimerStop) {
            if (toast.timeoutId) {
                window.clearTimeout(toast.timeoutId);
                toast.timeoutId = null;
            }
        }
    };
    ToasterContainerComponent.prototype.restartTimer = function (toast) {
        if (this.toasterconfig.mouseoverTimerStop) {
            if (!toast.timeoutId) {
                this.configureTimer(toast);
            }
        }
        else if (toast.timeoutId === null) {
            this.removeToast(toast);
        }
    };
    // private functions
    ToasterContainerComponent.prototype.registerSubscribers = function () {
        var _this = this;
        this.addToastSubscriber = this.toasterService.addToast.subscribe(function (toast) {
            _this.addToast(toast);
        });
        this.clearToastsSubscriber = this.toasterService.clearToasts.subscribe(function (clearWrapper) {
            _this.clearToasts(clearWrapper);
        });
    };
    ToasterContainerComponent.prototype.addToast = function (toast) {
        toast.toasterConfig = this.toasterconfig;
        if (toast.toastContainerId && this.toasterconfig.toastContainerId
            && toast.toastContainerId !== this.toasterconfig.toastContainerId) {
            return;
        }
        ;
        if (!toast.type) {
            toast.type = this.toasterconfig.defaultTypeClass;
        }
        if (this.toasterconfig.preventDuplicates && this.toasts.length > 0) {
            if (toast.toastId && this.toasts.some(function (t) { return t.toastId === toast.toastId; })) {
                return;
            }
            else if (this.toasts.some(function (t) { return t.body === toast.body; })) {
                return;
            }
        }
        if (toast.showCloseButton === null || typeof toast.showCloseButton === 'undefined') {
            if (typeof this.toasterconfig.showCloseButton === 'object') {
                toast.showCloseButton = this.toasterconfig.showCloseButton[toast.type];
            }
            else if (typeof this.toasterconfig.showCloseButton === 'boolean') {
                toast.showCloseButton = this.toasterconfig.showCloseButton;
            }
        }
        if (toast.showCloseButton) {
            toast.closeHtml = toast.closeHtml || this.toasterconfig.closeHtml;
        }
        toast.bodyOutputType = toast.bodyOutputType || this.toasterconfig.bodyOutputType;
        this.configureTimer(toast);
        if (this.toasterconfig.newestOnTop) {
            this.toasts.unshift(toast);
            if (this.isLimitExceeded()) {
                this.toasts.pop();
            }
        }
        else {
            this.toasts.push(toast);
            if (this.isLimitExceeded()) {
                this.toasts.shift();
            }
        }
        if (toast.onShowCallback) {
            toast.onShowCallback(toast);
        }
    };
    ToasterContainerComponent.prototype.configureTimer = function (toast) {
        var _this = this;
        var timeout = (typeof toast.timeout === 'number')
            ? toast.timeout : this.toasterconfig.timeout;
        if (typeof timeout === 'object') {
            timeout = timeout[toast.type];
        }
        ;
        if (timeout > 0) {
            this.ngZone.runOutsideAngular(function () {
                toast.timeoutId = window.setTimeout(function () {
                    _this.ngZone.run(function () {
                        _this.ref.markForCheck();
                        _this.removeToast(toast);
                    });
                }, timeout);
            });
        }
    };
    ToasterContainerComponent.prototype.isLimitExceeded = function () {
        return this.toasterconfig.limit && this.toasts.length > this.toasterconfig.limit;
    };
    ToasterContainerComponent.prototype.removeToast = function (toast) {
        var index = this.toasts.indexOf(toast);
        if (index < 0) {
            return;
        }
        ;
        this.toasts.splice(index, 1);
        if (toast.timeoutId) {
            window.clearTimeout(toast.timeoutId);
            toast.timeoutId = null;
        }
        if (toast.onHideCallback) {
            toast.onHideCallback(toast);
        }
        this.toasterService._removeToastSubject.next({ toastId: toast.toastId, toastContainerId: toast.toastContainerId });
    };
    ToasterContainerComponent.prototype.removeAllToasts = function () {
        for (var i = this.toasts.length - 1; i >= 0; i--) {
            this.removeToast(this.toasts[i]);
        }
    };
    ToasterContainerComponent.prototype.clearToasts = function (clearWrapper) {
        var toastId = clearWrapper.toastId;
        var toastContainerId = clearWrapper.toastContainerId;
        if (toastContainerId === null || typeof toastContainerId === 'undefined') {
            this.clearToastsAction(toastId);
        }
        else if (toastContainerId === this.toasterconfig.toastContainerId) {
            this.clearToastsAction(toastId);
        }
    };
    ToasterContainerComponent.prototype.clearToastsAction = function (toastId) {
        if (toastId) {
            this.removeToast(this.toasts.filter(function (t) { return t.toastId === toastId; })[0]);
        }
        else {
            this.removeAllToasts();
        }
    };
    ToasterContainerComponent.prototype.ngOnDestroy = function () {
        if (this.addToastSubscriber) {
            this.addToastSubscriber.unsubscribe();
        }
        if (this.clearToastsSubscriber) {
            this.clearToastsSubscriber.unsubscribe();
        }
    };
    return ToasterContainerComponent;
}());

ToasterContainerComponent.decorators = [
    { type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"], args: [{
                selector: 'toaster-container',
                template: "\n        <div id=\"toast-container\" [ngClass]=\"[toasterconfig.positionClass]\">\n            <div toastComp *ngFor=\"let toast of toasts\" class=\"toast\" [toast]=\"toast\"\n                [@toastState]=\"toasterconfig.animation\"\n                [iconClass]=\"toasterconfig.iconClasses[toast.type]\"\n                [ngClass]=\"toasterconfig.typeClasses[toast.type]\"\n                (click)=\"click(toast)\" (clickEvent)=\"childClick($event)\"\n                (mouseover)=\"stopTimer(toast)\" (mouseout)=\"restartTimer(toast)\">\n            </div>\n        </div>\n        ",
                // TODO: use styleUrls once Angular 2 supports the use of relative paths
                // https://github.com/angular/angular/issues/2383
                // styleUrls: ['./toaster.css']
                animations: [
                    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__angular_animations__["trigger"])('toastState', [
                        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__angular_animations__["state"])('flyRight, flyLeft, slideDown, slideUp, fade', __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__angular_animations__["style"])({ opacity: 1, transform: 'translate(0,0)' })),
                        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__angular_animations__["transition"])('void => flyRight', [
                            __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__angular_animations__["style"])({
                                opacity: 0, transform: 'translateX(100%)'
                            }),
                            __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__angular_animations__["animate"])('0.25s ease-in')
                        ]),
                        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__angular_animations__["transition"])('flyRight => void', [
                            __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__angular_animations__["animate"])('0.25s 10ms ease-out', __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__angular_animations__["style"])({
                                opacity: 0, transform: 'translateX(100%)'
                            }))
                        ]),
                        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__angular_animations__["transition"])('void => flyLeft', [
                            __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__angular_animations__["style"])({
                                opacity: 0, transform: 'translateX(-100%)'
                            }),
                            __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__angular_animations__["animate"])('0.25s ease-in')
                        ]),
                        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__angular_animations__["transition"])('flyLeft => void', [
                            __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__angular_animations__["animate"])('0.25s 10ms ease-out', __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__angular_animations__["style"])({
                                opacity: 0, transform: 'translateX(-100%)'
                            }))
                        ]),
                        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__angular_animations__["transition"])('void => slideDown', [
                            __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__angular_animations__["style"])({
                                opacity: 0, transform: 'translateY(-200%)'
                            }),
                            __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__angular_animations__["animate"])('0.3s ease-in')
                        ]),
                        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__angular_animations__["transition"])('slideDown => void', [
                            __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__angular_animations__["animate"])('0.3s 10ms ease-out', __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__angular_animations__["style"])({
                                opacity: 0, transform: 'translateY(200%)'
                            }))
                        ]),
                        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__angular_animations__["transition"])('void => slideUp', [
                            __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__angular_animations__["style"])({
                                opacity: 0, transform: 'translateY(200%)'
                            }),
                            __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__angular_animations__["animate"])('0.3s ease-in')
                        ]),
                        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__angular_animations__["transition"])('slideUp => void', [
                            __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__angular_animations__["animate"])('0.3s 10ms ease-out', __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__angular_animations__["style"])({
                                opacity: 0, transform: 'translateY(-200%)'
                            }))
                        ]),
                        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__angular_animations__["transition"])('void => fade', [
                            __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__angular_animations__["style"])({
                                opacity: 0,
                            }),
                            __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__angular_animations__["animate"])('0.3s ease-in')
                        ]),
                        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__angular_animations__["transition"])('fade => void', [
                            __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__angular_animations__["animate"])('0.3s 10ms ease-out', __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__angular_animations__["style"])({
                                opacity: 0,
                            }))
                        ])
                    ]),
                ]
            },] },
];
/** @nocollapse */
ToasterContainerComponent.ctorParameters = function () { return [
    { type: __WEBPACK_IMPORTED_MODULE_3__toaster_service__["ToasterService"], },
    { type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["ChangeDetectorRef"], },
    { type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["NgZone"], },
]; };
ToasterContainerComponent.propDecorators = {
    'toasterconfig': [{ type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"] },],
};
//# sourceMappingURL=toaster-container.component.js.map

/***/ }),
/* 12 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__angular_core__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1__angular_common__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__toast_component__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__toaster_container_component__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__toaster_service__ = __webpack_require__(5);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ToasterModule", function() { return ToasterModule; });





var ToasterModule = (function () {
    function ToasterModule() {
    }
    return ToasterModule;
}());

ToasterModule.decorators = [
    { type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"], args: [{
                imports: [__WEBPACK_IMPORTED_MODULE_1__angular_common__["CommonModule"]],
                declarations: [
                    __WEBPACK_IMPORTED_MODULE_2__toast_component__["ToastComponent"],
                    __WEBPACK_IMPORTED_MODULE_3__toaster_container_component__["ToasterContainerComponent"]
                ],
                providers: [__WEBPACK_IMPORTED_MODULE_4__toaster_service__["ToasterService"]],
                exports: [
                    __WEBPACK_IMPORTED_MODULE_3__toaster_container_component__["ToasterContainerComponent"],
                    __WEBPACK_IMPORTED_MODULE_2__toast_component__["ToastComponent"]
                ]
            },] },
];
/** @nocollapse */
ToasterModule.ctorParameters = function () { return []; };
//# sourceMappingURL=toaster.module.js.map

/***/ }),
/* 13 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__angular_core__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CookieService", function() { return CookieService; });
// This service is based on the `ng2-cookies` package which sadly is not a service and does
// not use `DOCUMENT` injection and therefore doesn't work well with AoT production builds.
// Package: https://github.com/BCJTI/ng2-cookies


var CookieService = (function () {
    function CookieService(
        // The type `Document` may not be used here. Although a fix is on its way,
        // we will go with `any` for now to support Angular 2.4.x projects.
        // Issue: https://github.com/angular/angular/issues/12631
        // Fix: https://github.com/angular/angular/pull/14894
        document) {
        this.document = document;
        // To avoid issues with server side prerendering, check if `document` is defined.
        this.documentIsAccessible = document !== undefined;
    }
    /**
     * @param name Cookie name
     * @returns {boolean}
     */
    CookieService.prototype.check = function (name) {
        if (!this.documentIsAccessible) {
            return false;
        }
        name = encodeURIComponent(name);
        var regExp = this.getCookieRegExp(name);
        var exists = regExp.test(this.document.cookie);
        return exists;
    };
    /**
     * @param name Cookie name
     * @returns {any}
     */
    CookieService.prototype.get = function (name) {
        if (this.documentIsAccessible && this.check(name)) {
            name = encodeURIComponent(name);
            var regExp = this.getCookieRegExp(name);
            var result = regExp.exec(this.document.cookie);
            return decodeURIComponent(result[1]);
        }
        else {
            return '';
        }
    };
    /**
     * @returns {}
     */
    CookieService.prototype.getAll = function () {
        if (!this.documentIsAccessible) {
            return {};
        }
        var cookies = {};
        var document = this.document;
        if (document.cookie && document.cookie !== '') {
            var split = document.cookie.split(';');
            for (var i = 0; i < split.length; i += 1) {
                var currentCookie = split[i].split('=');
                currentCookie[0] = currentCookie[0].replace(/^ /, '');
                cookies[decodeURIComponent(currentCookie[0])] = decodeURIComponent(currentCookie[1]);
            }
        }
        return cookies;
    };
    /**
     * @param name    Cookie name
     * @param value   Cookie value
     * @param expires Number of days until the cookies expires or an actual `Date`
     * @param path    Cookie path
     * @param domain  Cookie domain
     * @param secure  Secure flag
     */
    CookieService.prototype.set = function (name, value, expires, path, domain, secure) {
        if (!this.documentIsAccessible) {
            return;
        }
        var cookieString = encodeURIComponent(name) + '=' + encodeURIComponent(value) + ';';
        if (expires) {
            if (typeof expires === 'number') {
                var dateExpires = new Date(new Date().getTime() + expires * 1000 * 60 * 60 * 24);
                cookieString += 'expires=' + dateExpires.toUTCString() + ';';
            }
            else {
                cookieString += 'expires=' + expires.toUTCString() + ';';
            }
        }
        if (path) {
            cookieString += 'path=' + path + ';';
        }
        if (domain) {
            cookieString += 'domain=' + domain + ';';
        }
        if (secure) {
            cookieString += 'secure;';
        }
        this.document.cookie = cookieString;
    };
    /**
     * @param name   Cookie name
     * @param path   Cookie path
     * @param domain Cookie domain
     */
    CookieService.prototype.delete = function (name, path, domain) {
        if (!this.documentIsAccessible) {
            return;
        }
        this.set(name, '', -1, path, domain);
    };
    /**
     * @param path   Cookie path
     * @param domain Cookie domain
     */
    CookieService.prototype.deleteAll = function (path, domain) {
        if (!this.documentIsAccessible) {
            return;
        }
        var cookies = this.getAll();
        for (var cookieName in cookies) {
            if (cookies.hasOwnProperty(cookieName)) {
                this.delete(cookieName, path, domain);
            }
        }
    };
    /**
     * @param name Cookie name
     * @returns {RegExp}
     */
    CookieService.prototype.getCookieRegExp = function (name) {
        var escapedName = name.replace(/([\[\]\{\}\(\)\|\=\;\+\?\,\.\*\^\$])/ig, '\\$1');
        return new RegExp('(?:^' + escapedName + '|;\\s*' + escapedName + ')=(.*?)(?:;|$)', 'g');
    };
    return CookieService;
}());

CookieService.decorators = [
    { type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"] },
];
/** @nocollapse */
CookieService.ctorParameters = function () { return [
    { type: undefined, decorators: [{ type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["Inject"], args: [__WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__["DOCUMENT"],] },] },
]; };
//# sourceMappingURL=cookie.service.js.map

/***/ }),
/* 14 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/**
 * @fileoverview This file is generated by the Angular template compiler.
 * Do not edit.
 * @suppress {suspiciousCode,uselessCode,missingProperties,missingOverride}
 */
/* tslint:disable */

Object.defineProperty(exports, "__esModule", { value: true });
var i0 = __webpack_require__(51);
var i1 = __webpack_require__(0);
var i2 = __webpack_require__(1);
var i3 = __webpack_require__(49);
var i4 = __webpack_require__(11);
var i5 = __webpack_require__(5);
var i6 = __webpack_require__(7);
var i7 = __webpack_require__(3);
var styles_AppComponent = [i0.styles];
exports.RenderType_AppComponent = i1.ɵcrt({ encapsulation: 0, styles: styles_AppComponent,
    data: {} });
function View_AppComponent_0(_l) {
    return i1.ɵvid(0, [(_l()(), i1.ɵeld(0, 16777216, null, null, 1, 'router-outlet', [], null, null, null, null, null)),
        i1.ɵdid(1, 212992, null, 0, i2.RouterOutlet, [i2.ChildrenOutletContexts,
            i1.ViewContainerRef, i1.ComponentFactoryResolver, [8, null], i1.ChangeDetectorRef], null, null), (_l()(), i1.ɵted(-1, null, ['\n'])), (_l()(),
            i1.ɵeld(3, 0, null, null, 1, 'toaster-container', [], null, null, null, i3.View_ToasterContainerComponent_0, i3.RenderType_ToasterContainerComponent)), i1.ɵdid(4, 245760, null, 0, i4.ToasterContainerComponent, [i5.ToasterService, i1.ChangeDetectorRef, i1.NgZone], { toasterconfig: [0, 'toasterconfig'] }, null), (_l()(), i1.ɵted(-1, null, ['\n']))], function (_ck, _v) {
        var _co = _v.component;
        _ck(_v, 1, 0);
        var currVal_0 = _co.toasterconfig;
        _ck(_v, 4, 0, currVal_0);
    }, null);
}
exports.View_AppComponent_0 = View_AppComponent_0;
function View_AppComponent_Host_0(_l) {
    return i1.ɵvid(0, [(_l()(), i1.ɵeld(0, 0, null, null, 1, 'app-root', [], null, null, null, View_AppComponent_0, exports.RenderType_AppComponent)),
        i1.ɵdid(1, 114688, null, 0, i6.AppComponent, [i2.Router, i7.Meta, i7.Title], null, null)], function (_ck, _v) {
        _ck(_v, 1, 0);
    }, null);
}
exports.View_AppComponent_Host_0 = View_AppComponent_Host_0;
exports.AppComponentNgFactory = i1.ɵccf('app-root', i6.AppComponent, View_AppComponent_Host_0, {}, {}, []);



/***/ }),
/* 15 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(0);
var router_1 = __webpack_require__(1);
var angular2_cool_storage_1 = __webpack_require__(19);
var CanLoginActivate = (function () {
    function CanLoginActivate(localStorage, router) {
        this.localStorage = localStorage;
        this.router = router;
    }
    CanLoginActivate.prototype.canActivate = function (route, state) {
        //console.log('can login activate called')
        if (!this.localStorage.getItem("authToken")) {
            return true;
        }
        this.router.navigate(['/main']);
        return false;
    };
    return CanLoginActivate;
}());
CanLoginActivate = __decorate([
    core_1.Injectable(),
    __metadata("design:paramtypes", [angular2_cool_storage_1.CoolLocalStorage, router_1.Router])
], CanLoginActivate);
exports.CanLoginActivate = CanLoginActivate;
var AlwaysAuthChildrenGuard = (function () {
    function AlwaysAuthChildrenGuard(localStorage, router) {
        this.localStorage = localStorage;
        this.router = router;
    }
    AlwaysAuthChildrenGuard.prototype.canActivateChild = function (route, state) {
        //console.log('can child activate called')
        if (this.localStorage.getItem("authToken")) {
            return true;
        }
        this.router.navigate(['/login']);
        return false;
    };
    return AlwaysAuthChildrenGuard;
}());
AlwaysAuthChildrenGuard = __decorate([
    core_1.Injectable(),
    __metadata("design:paramtypes", [angular2_cool_storage_1.CoolLocalStorage, router_1.Router])
], AlwaysAuthChildrenGuard);
exports.AlwaysAuthChildrenGuard = AlwaysAuthChildrenGuard;


/***/ }),
/* 16 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(0);
var http_1 = __webpack_require__(4);
var Observable_1 = __webpack_require__(41);
__webpack_require__(62);
__webpack_require__(63);
var CameraService = (function () {
    function CameraService(http) {
        this.http = http;
        this.serviceUrl = 'http://localhost:61169/api/image';
    }
    CameraService.prototype.saveImage = function (imageData) {
        // console.log(imageData);
        var headers = new http_1.Headers({ 'Content-Type': 'application/json' });
        var options = new http_1.RequestOptions({ headers: headers, method: http_1.RequestMethod.Post });
        console.log('ska spara');
        // this.http.post(this.serviceUrl, { imageData }, options)
        //                 .map(this.extractData)
        //                 .catch(this.handleError);
        return this.http.post(this.serviceUrl, imageData, { headers: headers, method: http_1.RequestMethod.Post })
            .map(function (res) { return res.json(); })
            .catch(this.handleError);
    };
    CameraService.prototype.extractData = function (res) {
        var body = res.json();
        return body.data || {};
    };
    // private handleError (error: Response | any) {
    //   // In a real world app, we might use a remote logging infrastructure
    //   let errMsg: string;
    //   if (error instanceof Response) {
    //     const body = error.json() || '';
    //     const err = body.error || JSON.stringify(body);
    //     errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    //   } else {
    //     errMsg = error.message ? error.message : error.toString();
    //   }
    //   console.error(errMsg);
    //   return Observable.throw(errMsg);
    // }
    CameraService.prototype.handleError = function (error) {
        console.error('Error in the DepartureScheduleService');
        return Observable_1.Observable.throw(error.json().error || 'Server error');
    };
    return CameraService;
}());
CameraService = __decorate([
    core_1.Injectable(),
    __metadata("design:paramtypes", [http_1.Http])
], CameraService);
exports.CameraService = CameraService;
//     } else {
//         return this.http.post(serviceUrl, search, {headers: headers, method: RequestMethod.Post})
//              .map((res: Response) => res.json())
//              .catch(this.handleError);
//     }
// }


/***/ }),
/* 17 */
/***/ (function(module, exports) {

module.exports = require("@angular/forms");

/***/ }),
/* 18 */
/***/ (function(module, exports) {

module.exports = require("@angular/platform-browser/animations");

/***/ }),
/* 19 */
/***/ (function(module, exports) {

module.exports = require("angular2-cool-storage");

/***/ }),
/* 20 */
/***/ (function(module, exports) {

module.exports = require("@angular/platform-server");

/***/ }),
/* 21 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__src_toast_component__ = __webpack_require__(10);
/* harmony reexport (binding) */ __webpack_require__.d(__webpack_exports__, "ToastComponent", function() { return __WEBPACK_IMPORTED_MODULE_0__src_toast_component__["ToastComponent"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__src_toaster_container_component__ = __webpack_require__(11);
/* harmony reexport (binding) */ __webpack_require__.d(__webpack_exports__, "ToasterContainerComponent", function() { return __WEBPACK_IMPORTED_MODULE_1__src_toaster_container_component__["ToasterContainerComponent"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__src_toaster_service__ = __webpack_require__(5);
/* harmony reexport (binding) */ __webpack_require__.d(__webpack_exports__, "ToasterService", function() { return __WEBPACK_IMPORTED_MODULE_2__src_toaster_service__["ToasterService"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__src_toaster_config__ = __webpack_require__(33);
/* harmony reexport (binding) */ __webpack_require__.d(__webpack_exports__, "ToasterConfig", function() { return __WEBPACK_IMPORTED_MODULE_3__src_toaster_config__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__src_bodyOutputType__ = __webpack_require__(22);
/* harmony reexport (binding) */ __webpack_require__.d(__webpack_exports__, "BodyOutputType", function() { return __WEBPACK_IMPORTED_MODULE_4__src_bodyOutputType__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__src_toaster_module__ = __webpack_require__(12);
/* harmony reexport (binding) */ __webpack_require__.d(__webpack_exports__, "ToasterModule", function() { return __WEBPACK_IMPORTED_MODULE_5__src_toaster_module__["ToasterModule"]; });






//# sourceMappingURL=angular2-toaster.js.map

/***/ }),
/* 22 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BodyOutputType; });
var BodyOutputType;
(function (BodyOutputType) {
    BodyOutputType[BodyOutputType["Default"] = 0] = "Default";
    BodyOutputType[BodyOutputType["TrustedHtml"] = 1] = "TrustedHtml";
    BodyOutputType[BodyOutputType["Component"] = 2] = "Component";
})(BodyOutputType || (BodyOutputType = {}));
//# sourceMappingURL=bodyOutputType.js.map

/***/ }),
/* 23 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(0);
var camera_service_1 = __webpack_require__(16);
var commonComponent_1 = __webpack_require__(6);
var CameraComponent = (function (_super) {
    __extends(CameraComponent, _super);
    function CameraComponent(injector, cameraService) {
        var _this = _super.call(this, injector) || this;
        _this.cameraService = cameraService;
        _this.showVideo = false; // Fungerar fast tvärtom. Byt logik
        return _this;
    }
    CameraComponent.prototype.capture = function () {
        // this.context = this.canvas.nativeElement.getContext('2d');
        this.context.drawImage(this.videoPlayer.nativeElement, 0, 0, this.width, this.height);
        this.showVideo = true;
    };
    CameraComponent.prototype.saveImage = function () {
        this.showVideo = false;
        var imgData = this.canvas.nativeElement.toDataURL('img/png');
        // console.log(imgData);
        this.myImg = imgData;
        imgData = imgData.replace('data:image/png;base64,', '');
        var postData = JSON.stringify({
            'ImageBase64String': imgData,
            'id': 3
        });
        console.log(postData);
        this.setToken('motor_img_url', this.myImg);
        this.popToast('success', 'Uploaded', 'Image Uploaded Successfully');
        //Call api here()
    };
    CameraComponent.prototype.ngOnInit = function () {
        this.width = 400;
        this.height = 300;
    };
    CameraComponent.prototype.ngAfterViewInit = function () {
        var _this = this;
        this.context = this.canvas.nativeElement.getContext('2d');
        if (navigator.mediaDevices && navigator.mediaDevices.getUserMedia) {
            setTimeout(function () {
                navigator.mediaDevices.getUserMedia({ video: true }).then(function (stream) {
                    if ("srcObject" in _this.videoPlayer.nativeElement) {
                        _this.videoPlayer.nativeElement.srcObject = stream;
                    }
                    else {
                        _this.videoPlayer.nativeElement.src = window.URL.createObjectURL(stream);
                    }
                    _this.videoPlayer.nativeElement.onloadedmetadata = function (e) {
                        if (this.videoPlayer && this.videoPlayer.nativeElement) {
                            this.videoPlayer.nativeElement.play();
                        }
                    };
                }).catch(function (err) {
                    alert('Error:' + err.message);
                });
            }, 2000);
        }
        else {
            alert("Doesn't support devices.");
        }
    };
    return CameraComponent;
}(commonComponent_1.BaseComponent));
__decorate([
    core_1.ViewChild('videoplayer'),
    __metadata("design:type", Object)
], CameraComponent.prototype, "videoPlayer", void 0);
__decorate([
    core_1.ViewChild('canvas'),
    __metadata("design:type", Object)
], CameraComponent.prototype, "canvas", void 0);
__decorate([
    core_1.Input(),
    __metadata("design:type", Number)
], CameraComponent.prototype, "width", void 0);
__decorate([
    core_1.Input(),
    __metadata("design:type", Number)
], CameraComponent.prototype, "height", void 0);
CameraComponent = __decorate([
    core_1.Component({
        selector: 'app-camera',
        templateUrl: './camera.component.html',
        styleUrls: ['./camera.component.css'],
        providers: [camera_service_1.CameraService]
    }),
    __metadata("design:paramtypes", [core_1.Injector, camera_service_1.CameraService])
], CameraComponent);
exports.CameraComponent = CameraComponent;


/***/ }),
/* 24 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(0);
var core_2 = __webpack_require__(0);
var commonComponent_1 = __webpack_require__(6);
var DetailComponent = (function (_super) {
    __extends(DetailComponent, _super);
    function DetailComponent(injector) {
        return _super.call(this, injector) || this;
    }
    DetailComponent.prototype.ngOnInit = function () {
        this.discData = JSON.parse(this.getToken('disc_data'));
        console.log('disc Data : ', this.discData);
        //this.popToast('success','Welcome','Successfully logged in.')
    };
    return DetailComponent;
}(commonComponent_1.BaseComponent));
DetailComponent = __decorate([
    core_1.Component({
        selector: 'app-detail',
        templateUrl: './detail.component.html'
    }),
    __metadata("design:paramtypes", [core_2.Injector])
], DetailComponent);
exports.DetailComponent = DetailComponent;


/***/ }),
/* 25 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(0);
var core_2 = __webpack_require__(0);
var commonComponent_1 = __webpack_require__(6);
var HomePageComponent = (function (_super) {
    __extends(HomePageComponent, _super);
    function HomePageComponent(injector) {
        return _super.call(this, injector) || this;
    }
    HomePageComponent.prototype.ngOnInit = function () {
        //this.popToast('success','Welcome','Successfully logged in.')
        this.showLoader = false;
        console.log('showLoader', this.showLoader);
    };
    HomePageComponent.prototype.onFileChange = function (event) {
        var _this = this;
        var reader = new FileReader();
        if (event.target.files && event.target.files.length > 0) {
            var file_1 = event.target.files[0];
            reader.readAsDataURL(file_1);
            reader.onload = function () {
                console.log('file', file_1);
                //Call file upload api here 
                //onsuccess redirect to next step
                var fd = new FormData();
                fd.append('image', file_1);
                _this.showLoader = true;
                _this.commonService.uploadDisc(fd).subscribe(function (success) {
                    _this.showLoader = false;
                    if (success.success == 1) {
                        var discData = {
                            licenseNo: success[6],
                            vehRegNo: success[7],
                            description: success[8],
                            make: success[9],
                            vin: success[12],
                            engineNo: success[13]
                        };
                        _this.setToken('disc_data', JSON.stringify(discData));
                        _this.router.navigate(['/main/detail']);
                    }
                    else if (success.success == 0) {
                        _this.popToast('error', 'Error', 'Please Scan the disc properly.');
                    }
                }, function (error) {
                    _this.showLoader = false;
                    console.log('error', error);
                    _this.popToast('error', 'Error', 'Please Scan the disc properly.');
                });
            };
        }
    };
    return HomePageComponent;
}(commonComponent_1.BaseComponent));
HomePageComponent = __decorate([
    core_1.Component({
        selector: 'app-home-page',
        templateUrl: './home-page.component.html'
    }),
    __metadata("design:paramtypes", [core_2.Injector])
], HomePageComponent);
exports.HomePageComponent = HomePageComponent;


/***/ }),
/* 26 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(0);
var core_2 = __webpack_require__(0);
var commonComponent_1 = __webpack_require__(6);
var MainComponent = (function (_super) {
    __extends(MainComponent, _super);
    function MainComponent(injector) {
        var _this = _super.call(this, injector) || this;
        console.log('location', {"protocol":"https","host":"localhost"});
        return _this;
    }
    MainComponent.prototype.ngOnInit = function () {
    };
    return MainComponent;
}(commonComponent_1.BaseComponent));
MainComponent = __decorate([
    core_1.Component({
        selector: 'app-main',
        templateUrl: './main.component.html'
    }),
    __metadata("design:paramtypes", [core_2.Injector])
], MainComponent);
exports.MainComponent = MainComponent;


/***/ }),
/* 27 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(0);
var core_2 = __webpack_require__(0);
var commonComponent_1 = __webpack_require__(6);
var ThankyouComponent = (function (_super) {
    __extends(ThankyouComponent, _super);
    function ThankyouComponent(injector) {
        return _super.call(this, injector) || this;
    }
    ThankyouComponent.prototype.ngOnInit = function () {
        //this.popToast('success','Welcome','Successfully logged in.')
    };
    return ThankyouComponent;
}(commonComponent_1.BaseComponent));
ThankyouComponent = __decorate([
    core_1.Component({
        selector: 'app-thankyou',
        templateUrl: './thankyou.component.html'
    }),
    __metadata("design:paramtypes", [core_2.Injector])
], ThankyouComponent);
exports.ThankyouComponent = ThankyouComponent;


/***/ }),
/* 28 */
/***/ (function(module, exports) {

module.exports = require("@angular/animations");

/***/ }),
/* 29 */
/***/ (function(module, exports) {

module.exports = require("angular2-cool-storage/index");

/***/ }),
/* 30 */
/***/ (function(module, exports) {

module.exports = require("angular2-cool-storage/src/cool-local-storage");

/***/ }),
/* 31 */
/***/ (function(module, exports) {

module.exports = require("angular2-cool-storage/src/cool-session-storage");

/***/ }),
/* 32 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/**
 * @fileoverview This file is generated by the Angular template compiler.
 * Do not edit.
 * @suppress {suspiciousCode,uselessCode,missingProperties,missingOverride}
 */
/* tslint:disable */

Object.defineProperty(exports, "__esModule", { value: true });
var i0 = __webpack_require__(0);
var i1 = __webpack_require__(57);
var i2 = __webpack_require__(7);
var i3 = __webpack_require__(14);
var i4 = __webpack_require__(4);
var i5 = __webpack_require__(20);
var i6 = __webpack_require__(60);
var i7 = __webpack_require__(2);
var i8 = __webpack_require__(3);
var i9 = __webpack_require__(40);
var i10 = __webpack_require__(18);
var i11 = __webpack_require__(28);
var i12 = __webpack_require__(30);
var i13 = __webpack_require__(31);
var i14 = __webpack_require__(5);
var i15 = __webpack_require__(17);
var i16 = __webpack_require__(1);
var i17 = __webpack_require__(15);
var i18 = __webpack_require__(13);
var i19 = __webpack_require__(9);
var i20 = __webpack_require__(29);
var i21 = __webpack_require__(12);
var i22 = __webpack_require__(8);
exports.AppServerModuleNgFactory = i0.ɵcmf(i1.AppServerModule, [i2.AppComponent], function (_l) {
    return i0.ɵmod([i0.ɵmpd(512, i0.ComponentFactoryResolver, i0.ɵCodegenComponentFactoryResolver, [[8, [i3.AppComponentNgFactory]], [3, i0.ComponentFactoryResolver], i0.NgModuleRef]),
        i0.ɵmpd(4608, i4.BrowserXhr, i5.ɵc, []), i0.ɵmpd(4608, i4.ResponseOptions, i4.BaseResponseOptions, []), i0.ɵmpd(4608, i4.XSRFStrategy, i5.ɵd, []), i0.ɵmpd(4608, i4.XHRBackend, i4.XHRBackend, [i4.BrowserXhr,
            i4.ResponseOptions, i4.XSRFStrategy]), i0.ɵmpd(4608, i4.RequestOptions, i4.BaseRequestOptions, []), i0.ɵmpd(5120, i4.Http, i5.ɵe, [i4.XHRBackend,
            i4.RequestOptions]), i0.ɵmpd(4608, i6.HttpXsrfTokenExtractor, i6.ɵg, [i7.DOCUMENT,
            i0.PLATFORM_ID, i6.ɵe]), i0.ɵmpd(4608, i6.ɵh, i6.ɵh, [i6.HttpXsrfTokenExtractor,
            i6.ɵf]), i0.ɵmpd(5120, i6.HTTP_INTERCEPTORS, function (p0_0) {
            return [p0_0];
        }, [i6.ɵh]), i0.ɵmpd(4608, i6.XhrFactory, i5.ɵc, []), i0.ɵmpd(4608, i6.HttpXhrBackend, i6.HttpXhrBackend, [i6.XhrFactory]), i0.ɵmpd(6144, i6.HttpBackend, null, [i6.HttpXhrBackend]), i0.ɵmpd(5120, i6.HttpHandler, i5.ɵf, [i6.HttpBackend, [2, i6.HTTP_INTERCEPTORS]]), i0.ɵmpd(4608, i6.HttpClient, i6.HttpClient, [i6.HttpHandler]), i0.ɵmpd(4608, i6.ɵd, i6.ɵd, []),
        i0.ɵmpd(5120, i0.LOCALE_ID, i0.ɵm, [[3, i0.LOCALE_ID]]), i0.ɵmpd(4608, i7.NgLocalization, i7.NgLocaleLocalization, [i0.LOCALE_ID]), i0.ɵmpd(5120, i0.IterableDiffers, i0.ɵk, []), i0.ɵmpd(5120, i0.KeyValueDiffers, i0.ɵl, []),
        i0.ɵmpd(4608, i8.DomSanitizer, i8.ɵe, [i7.DOCUMENT]), i0.ɵmpd(6144, i0.Sanitizer, null, [i8.DomSanitizer]), i0.ɵmpd(4608, i8.HAMMER_GESTURE_CONFIG, i8.HammerGestureConfig, []), i0.ɵmpd(5120, i8.EVENT_MANAGER_PLUGINS, function (p0_0, p1_0, p2_0, p2_1) {
            return [new i8.ɵDomEventsPlugin(p0_0), new i8.ɵKeyEventsPlugin(p1_0),
                new i8.ɵHammerGesturesPlugin(p2_0, p2_1)];
        }, [i7.DOCUMENT, i7.DOCUMENT, i7.DOCUMENT, i8.HAMMER_GESTURE_CONFIG]), i0.ɵmpd(4608, i8.EventManager, i8.EventManager, [i8.EVENT_MANAGER_PLUGINS, i0.NgZone]),
        i0.ɵmpd(135680, i8.ɵDomSharedStylesHost, i8.ɵDomSharedStylesHost, [i7.DOCUMENT]),
        i0.ɵmpd(4608, i8.ɵDomRendererFactory2, i8.ɵDomRendererFactory2, [i8.EventManager,
            i8.ɵDomSharedStylesHost]), i0.ɵmpd(4608, i5.ɵb, i5.ɵb, [i8.DOCUMENT, [2, i8.ɵTRANSITION_ID]]),
        i0.ɵmpd(6144, i8.ɵSharedStylesHost, null, [i5.ɵb]), i0.ɵmpd(4608, i5.ɵServerRendererFactory2, i5.ɵServerRendererFactory2, [i0.NgZone, i8.DOCUMENT, i8.ɵSharedStylesHost]),
        i0.ɵmpd(4608, i9.AnimationDriver, i9.ɵNoopAnimationDriver, []), i0.ɵmpd(5120, i9.ɵAnimationStyleNormalizer, i10.ɵd, []), i0.ɵmpd(4608, i9.ɵAnimationEngine, i10.ɵb, [i9.AnimationDriver, i9.ɵAnimationStyleNormalizer]), i0.ɵmpd(5120, i0.RendererFactory2, i5.ɵa, [i5.ɵServerRendererFactory2, i9.ɵAnimationEngine,
            i0.NgZone]), i0.ɵmpd(4352, i0.Testability, null, []),
        i0.ɵmpd(4608, i8.Meta, i8.Meta, [i7.DOCUMENT]), i0.ɵmpd(4608, i8.Title, i8.Title, [i7.DOCUMENT]), i0.ɵmpd(4608, i11.AnimationBuilder, i10.ɵBrowserAnimationBuilder, [i0.RendererFactory2, i8.DOCUMENT]), i0.ɵmpd(4608, i12.CoolLocalStorage, i12.CoolLocalStorage, [i0.PLATFORM_ID]), i0.ɵmpd(4608, i13.CoolSessionStorage, i13.CoolSessionStorage, [i0.PLATFORM_ID]), i0.ɵmpd(4608, i14.ToasterService, i14.ToasterService, []), i0.ɵmpd(4608, i15.ɵi, i15.ɵi, []),
        i0.ɵmpd(5120, i16.ActivatedRoute, i16.ɵf, [i16.Router]), i0.ɵmpd(4608, i16.NoPreloading, i16.NoPreloading, []), i0.ɵmpd(6144, i16.PreloadingStrategy, null, [i16.NoPreloading]), i0.ɵmpd(135680, i16.RouterPreloader, i16.RouterPreloader, [i16.Router, i0.NgModuleFactoryLoader, i0.Compiler,
            i0.Injector, i16.PreloadingStrategy]), i0.ɵmpd(4608, i16.PreloadAllModules, i16.PreloadAllModules, []), i0.ɵmpd(5120, i16.ROUTER_INITIALIZER, i16.ɵi, [i16.ɵg]), i0.ɵmpd(5120, i0.APP_BOOTSTRAP_LISTENER, function (p0_0) {
            return [p0_0];
        }, [i16.ROUTER_INITIALIZER]), i0.ɵmpd(4608, i17.AlwaysAuthChildrenGuard, i17.AlwaysAuthChildrenGuard, [i12.CoolLocalStorage, i16.Router]), i0.ɵmpd(4608, i17.CanLoginActivate, i17.CanLoginActivate, [i12.CoolLocalStorage, i16.Router]), i0.ɵmpd(4608, i18.CookieService, i18.CookieService, [i8.DOCUMENT]), i0.ɵmpd(4608, i19.CommonService, i19.CommonService, [i0.Injector, i12.CoolLocalStorage, i4.Http]), i0.ɵmpd(512, i4.HttpModule, i4.HttpModule, []), i0.ɵmpd(512, i6.HttpClientXsrfModule, i6.HttpClientXsrfModule, []), i0.ɵmpd(512, i6.HttpClientModule, i6.HttpClientModule, []), i0.ɵmpd(512, i7.CommonModule, i7.CommonModule, []), i0.ɵmpd(1024, i0.ErrorHandler, i8.ɵa, []), i0.ɵmpd(1024, i0.NgProbeToken, function () {
            return [i16.ɵb()];
        }, []), i0.ɵmpd(256, i0.APP_ID, 'vin-frontend-ng4', []),
        i0.ɵmpd(2048, i8.ɵTRANSITION_ID, null, [i0.APP_ID]), i0.ɵmpd(512, i16.ɵg, i16.ɵg, [i0.Injector]), i0.ɵmpd(1024, i0.APP_INITIALIZER, function (p0_0, p0_1, p1_0, p1_1, p1_2, p2_0) {
            return [i8.ɵc(p0_0, p0_1), i8.ɵf(p1_0, p1_1, p1_2), i16.ɵh(p2_0)];
        }, [[2, i8.NgProbeToken], [2, i0.NgProbeToken], i8.ɵTRANSITION_ID, i7.DOCUMENT,
            i0.Injector, i16.ɵg]), i0.ɵmpd(512, i0.ApplicationInitStatus, i0.ApplicationInitStatus, [[2, i0.APP_INITIALIZER]]), i0.ɵmpd(131584, i0.ɵe, i0.ɵe, [i0.NgZone, i0.ɵConsole,
            i0.Injector, i0.ErrorHandler, i0.ComponentFactoryResolver, i0.ApplicationInitStatus]),
        i0.ɵmpd(2048, i0.ApplicationRef, null, [i0.ɵe]), i0.ɵmpd(512, i0.ApplicationModule, i0.ApplicationModule, [i0.ApplicationRef]), i0.ɵmpd(512, i8.BrowserModule, i8.BrowserModule, [[3, i8.BrowserModule]]), i0.ɵmpd(512, i10.NoopAnimationsModule, i10.NoopAnimationsModule, []), i0.ɵmpd(512, i5.ServerModule, i5.ServerModule, []), i0.ɵmpd(512, i20.CoolStorageModule, i20.CoolStorageModule, []), i0.ɵmpd(512, i21.ToasterModule, i21.ToasterModule, []),
        i0.ɵmpd(512, i15.ɵba, i15.ɵba, []), i0.ɵmpd(512, i15.FormsModule, i15.FormsModule, []), i0.ɵmpd(1024, i16.ɵa, i16.ɵd, [[3, i16.Router]]), i0.ɵmpd(512, i16.UrlSerializer, i16.DefaultUrlSerializer, []), i0.ɵmpd(512, i16.ChildrenOutletContexts, i16.ChildrenOutletContexts, []),
        i0.ɵmpd(256, i16.ROUTER_CONFIGURATION, { initialNavigation: 'enabled', useHash: false }, []), i0.ɵmpd(1024, i7.LocationStrategy, i16.ɵc, [i7.PlatformLocation,
            [2, i7.APP_BASE_HREF], i16.ROUTER_CONFIGURATION]), i0.ɵmpd(512, i7.Location, i7.Location, [i7.LocationStrategy]), i0.ɵmpd(512, i0.NgModuleFactoryLoader, i1.ServerFactoryLoader, []), i0.ɵmpd(512, i0.Compiler, i0.Compiler, []), i0.ɵmpd(1024, i16.ROUTES, function () {
            return [[{ path: 'main', loadChildren: './main/main.module#MainModule' }, { path: '',
                        redirectTo: 'main', pathMatch: 'full' }, { path: '**', redirectTo: 'main', pathMatch: 'full' }]];
        }, []), i0.ɵmpd(1024, i16.Router, i16.ɵe, [i0.ApplicationRef, i16.UrlSerializer,
            i16.ChildrenOutletContexts, i7.Location, i0.Injector, i0.NgModuleFactoryLoader,
            i0.Compiler, i16.ROUTES, i16.ROUTER_CONFIGURATION, [2, i16.UrlHandlingStrategy],
            [2, i16.RouteReuseStrategy]]), i0.ɵmpd(512, i16.RouterModule, i16.RouterModule, [[2, i16.ɵa], [2, i16.Router]]), i0.ɵmpd(512, i22.AppModule, i22.AppModule, []), i0.ɵmpd(512, i1.AppServerModule, i1.AppServerModule, []),
        i0.ɵmpd(256, i6.ɵe, 'XSRF-TOKEN', []), i0.ɵmpd(256, i6.ɵf, 'X-XSRF-TOKEN', [])]);
});



/***/ }),
/* 33 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__bodyOutputType__ = __webpack_require__(22);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ToasterConfig; });

var ToasterConfig = (function () {
    function ToasterConfig(configOverrides) {
        configOverrides = configOverrides || {};
        this.limit = configOverrides.limit || null;
        this.tapToDismiss = configOverrides.tapToDismiss != null ? configOverrides.tapToDismiss : true;
        this.showCloseButton = configOverrides.showCloseButton != null ? configOverrides.showCloseButton : false;
        this.closeHtml = configOverrides.closeHtml || '<button class="toast-close-button" type="button">&times;</button>';
        this.newestOnTop = configOverrides.newestOnTop != null ? configOverrides.newestOnTop : true;
        this.timeout = configOverrides.timeout != null ? configOverrides.timeout : 5000;
        this.typeClasses = configOverrides.typeClasses || {
            error: 'toast-error',
            info: 'toast-info',
            wait: 'toast-wait',
            success: 'toast-success',
            warning: 'toast-warning'
        };
        this.iconClasses = configOverrides.iconClasses || {
            error: 'icon-error',
            info: 'icon-info',
            wait: 'icon-wait',
            success: 'icon-success',
            warning: 'icon-warning'
        };
        this.bodyOutputType = configOverrides.bodyOutputType || __WEBPACK_IMPORTED_MODULE_0__bodyOutputType__["a" /* BodyOutputType */].Default;
        this.bodyTemplate = configOverrides.bodyTemplate || 'toasterBodyTmpl.html';
        this.defaultTypeClass = configOverrides.defaultTypeClass || 'toast-info';
        this.positionClass = configOverrides.positionClass || 'toast-top-right';
        this.titleClass = configOverrides.titleClass || 'toast-title';
        this.messageClass = configOverrides.messageClass || 'toast-message';
        this.animation = configOverrides.animation || '';
        this.preventDuplicates = configOverrides.preventDuplicates != null ? configOverrides.preventDuplicates : false;
        this.mouseoverTimerStop = configOverrides.mouseoverTimerStop != null ? configOverrides.mouseoverTimerStop : false;
        this.toastContainerId = configOverrides.toastContainerId != null ? configOverrides.toastContainerId : null;
    }
    return ToasterConfig;
}());

//# sourceMappingURL=toaster-config.js.map

/***/ }),
/* 34 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__cookie_service_cookie_service__ = __webpack_require__(13);
/* harmony namespace reexport (by provided) */ __webpack_require__.d(__webpack_exports__, "CookieService", function() { return __WEBPACK_IMPORTED_MODULE_0__cookie_service_cookie_service__["CookieService"]; });

//# sourceMappingURL=index.js.map

/***/ }),
/* 35 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/**
 * @fileoverview This file is generated by the Angular template compiler.
 * Do not edit.
 * @suppress {suspiciousCode,uselessCode,missingProperties,missingOverride}
 */
/* tslint:disable */

Object.defineProperty(exports, "__esModule", { value: true });
var i0 = __webpack_require__(54);
var i1 = __webpack_require__(0);
var i2 = __webpack_require__(2);
var i3 = __webpack_require__(23);
var i4 = __webpack_require__(16);
var i5 = __webpack_require__(4);
var styles_CameraComponent = [i0.styles];
exports.RenderType_CameraComponent = i1.ɵcrt({ encapsulation: 0,
    styles: styles_CameraComponent, data: {} });
function View_CameraComponent_1(_l) {
    return i1.ɵvid(0, [(_l()(), i1.ɵeld(0, 0, null, null, 1, 'a', [['class',
                'btn-capture pink'], ['href', 'javascript:;'], ['id', 'snap']], null, [[null,
                'click']], function (_v, en, $event) {
            var ad = true;
            var _co = _v.component;
            if (('click' === en)) {
                var pd_0 = (_co.capture() !== false);
                ad = (pd_0 && ad);
            }
            return ad;
        }, null, null)), (_l()(), i1.ɵeld(1, 0, null, null, 0, 'i', [['aria-hidden', 'true'], ['class', 'fa fa-camera']], null, null, null, null, null))], null, null);
}
function View_CameraComponent_2(_l) {
    return i1.ɵvid(0, [(_l()(), i1.ɵeld(0, 0, null, null, 1, 'a', [['class',
                'btn-capture green'], ['href', 'javascript:;'], ['id', 'save']], null, [[null,
                'click']], function (_v, en, $event) {
            var ad = true;
            var _co = _v.component;
            if (('click' === en)) {
                var pd_0 = (_co.saveImage() !== false);
                ad = (pd_0 && ad);
            }
            return ad;
        }, null, null)), (_l()(), i1.ɵeld(1, 0, null, null, 0, 'i', [['aria-hidden', 'true'], ['class', 'fa fa-check']], null, null, null, null, null))], null, null);
}
function View_CameraComponent_0(_l) {
    return i1.ɵvid(0, [i1.ɵqud(402653184, 1, { videoPlayer: 0 }), i1.ɵqud(402653184, 2, { canvas: 0 }),
        (_l()(), i1.ɵted(-1, null, ['\n'])), (_l()(), i1.ɵeld(3, 0, null, null, 18, 'div', [['class', 'camera-container']], null, null, null, null, null)), (_l()(), i1.ɵted(-1, null, ['\n	'])), (_l()(), i1.ɵeld(5, 0, null, null, 15, 'div', [['class',
                'video-wrapper']], null, null, null, null, null)), (_l()(), i1.ɵted(-1, null, ['\n		'])), (_l()(), i1.ɵeld(7, 0, null, null, 5, 'div', [], [[8, 'hidden', 0]], null, null, null, null)), (_l()(), i1.ɵted(-1, null, ['\n			'])), (_l()(), i1.ɵeld(9, 0, [[1, 0], ['videoplayer', 1]], null, 0, 'video', [['autoplay', '']], [[8, 'width', 0], [8, 'height', 0]], null, null, null, null)), (_l()(), i1.ɵted(-1, null, [' '])), (_l()(), i1.ɵeld(11, 0, null, null, 0, 'br', [], null, null, null, null, null)),
        (_l()(), i1.ɵted(-1, null, ['\n		'])), (_l()(), i1.ɵted(-1, null, ['\n\n		'])), (_l()(), i1.ɵeld(14, 0, null, null, 5, 'div', [], [[8, 'hidden', 0]], null, null, null, null)),
        (_l()(), i1.ɵted(-1, null, ['\n			'])), (_l()(), i1.ɵeld(16, 0, [[2, 0], ['canvas',
                1]], null, 0, 'canvas', [], [[8, 'width', 0], [8, 'height', 0]], null, null, null, null)), (_l()(), i1.ɵted(-1, null, [' '])), (_l()(), i1.ɵeld(18, 0, null, null, 0, 'br', [], null, null, null, null, null)), (_l()(), i1.ɵted(-1, null, ['\n		'])), (_l()(), i1.ɵted(-1, null, ['\n	'])), (_l()(), i1.ɵted(-1, null, ['\n'])), (_l()(),
            i1.ɵted(-1, null, ['\n\n'])), (_l()(), i1.ɵeld(23, 0, null, null, 7, 'div', [['class', 'bottom-button']], null, null, null, null, null)), (_l()(), i1.ɵted(-1, null, ['\n	'])),
        (_l()(), i1.ɵand(16777216, null, null, 1, null, View_CameraComponent_1)),
        i1.ɵdid(26, 16384, null, 0, i2.NgIf, [i1.ViewContainerRef, i1.TemplateRef], { ngIf: [0, 'ngIf'] }, null), (_l()(), i1.ɵted(-1, null, ['\n	'])),
        (_l()(), i1.ɵand(16777216, null, null, 1, null, View_CameraComponent_2)),
        i1.ɵdid(29, 16384, null, 0, i2.NgIf, [i1.ViewContainerRef, i1.TemplateRef], { ngIf: [0, 'ngIf'] }, null), (_l()(), i1.ɵted(-1, null, ['\n'])),
        (_l()(), i1.ɵted(-1, null, ['\n\n']))], function (_ck, _v) {
        var _co = _v.component;
        var currVal_6 = !_co.showVideo;
        _ck(_v, 26, 0, currVal_6);
        var currVal_7 = _co.showVideo;
        _ck(_v, 29, 0, currVal_7);
    }, function (_ck, _v) {
        var _co = _v.component;
        var currVal_0 = _co.showVideo;
        _ck(_v, 7, 0, currVal_0);
        var currVal_1 = _co.width;
        var currVal_2 = _co.height;
        _ck(_v, 9, 0, currVal_1, currVal_2);
        var currVal_3 = !_co.showVideo;
        _ck(_v, 14, 0, currVal_3);
        var currVal_4 = _co.width;
        var currVal_5 = _co.height;
        _ck(_v, 16, 0, currVal_4, currVal_5);
    });
}
exports.View_CameraComponent_0 = View_CameraComponent_0;
function View_CameraComponent_Host_0(_l) {
    return i1.ɵvid(0, [(_l()(), i1.ɵeld(0, 0, null, null, 2, 'app-camera', [], null, null, null, View_CameraComponent_0, exports.RenderType_CameraComponent)), i1.ɵprd(512, null, i4.CameraService, i4.CameraService, [i5.Http]), i1.ɵdid(2, 4308992, null, 0, i3.CameraComponent, [i1.Injector,
            i4.CameraService], null, null)], function (_ck, _v) {
        _ck(_v, 2, 0);
    }, null);
}
exports.View_CameraComponent_Host_0 = View_CameraComponent_Host_0;
exports.CameraComponentNgFactory = i1.ɵccf('app-camera', i3.CameraComponent, View_CameraComponent_Host_0, { width: 'width', height: 'height' }, {}, []);



/***/ }),
/* 36 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/**
 * @fileoverview This file is generated by the Angular template compiler.
 * Do not edit.
 * @suppress {suspiciousCode,uselessCode,missingProperties,missingOverride}
 */
/* tslint:disable */

Object.defineProperty(exports, "__esModule", { value: true });
var i0 = __webpack_require__(0);
var i1 = __webpack_require__(1);
var i2 = __webpack_require__(24);
var styles_DetailComponent = [];
exports.RenderType_DetailComponent = i0.ɵcrt({ encapsulation: 2,
    styles: styles_DetailComponent, data: {} });
function View_DetailComponent_0(_l) {
    return i0.ɵvid(0, [(_l()(), i0.ɵeld(0, 0, null, null, 41, 'div', [['class',
                'container']], null, null, null, null, null)),
        (_l()(), i0.ɵted(-1, null, ['\n	'])), (_l()(), i0.ɵeld(2, 0, null, null, 38, 'form', [], null, null, null, null, null)), (_l()(), i0.ɵted(-1, null, ['\n		'])),
        (_l()(), i0.ɵeld(4, 0, null, null, 4, 'div', [['class', 'message-block detail']], null, null, null, null, null)),
        (_l()(), i0.ɵted(-1, null, ['\n		'])), (_l()(), i0.ɵeld(6, 0, null, null, 1, 'h3', [], null, null, null, null, null)), (_l()(), i0.ɵted(-1, null, ['Your License disc details are the following:'])),
        (_l()(), i0.ɵted(-1, null, ['\n		'])), (_l()(), i0.ɵted(-1, null, ['\n		'])), (_l()(), i0.ɵeld(10, 0, null, null, 16, 'div', [['class',
                'licenece-detail']], null, null, null, null, null)), (_l()(), i0.ɵted(-1, null, ['\n		'])), (_l()(), i0.ɵeld(12, 0, null, null, 3, 'p', [], null, null, null, null, null)), (_l()(), i0.ɵted(-1, null, ['Engine No:'])), (_l()(), i0.ɵeld(14, 0, null, null, 1, 'span', [], null, null, null, null, null)),
        (_l()(), i0.ɵted(15, null, [' ', ' '])), (_l()(), i0.ɵted(-1, null, ['\n		'])), (_l()(), i0.ɵeld(17, 0, null, null, 3, 'p', [], null, null, null, null, null)),
        (_l()(), i0.ɵted(-1, null, ['VIN No:'])), (_l()(), i0.ɵeld(19, 0, null, null, 1, 'span', [], null, null, null, null, null)), (_l()(), i0.ɵted(20, null, [' ', ' '])),
        (_l()(), i0.ɵted(-1, null, ['\n		'])), (_l()(), i0.ɵeld(22, 0, null, null, 3, 'p', [], null, null, null, null, null)), (_l()(), i0.ɵted(-1, null, ['Reg No:'])),
        (_l()(), i0.ɵeld(24, 0, null, null, 1, 'span', [], null, null, null, null, null)), (_l()(), i0.ɵted(25, null, [' ', ' '])), (_l()(), i0.ɵted(-1, null, ['\n		'])), (_l()(),
            i0.ɵted(-1, null, ['\n		'])), (_l()(), i0.ɵeld(28, 0, null, null, 6, 'div', [['class', 'lbl-conf']], null, null, null, null, null)), (_l()(), i0.ɵted(-1, null, ['\n		\n			\n			'])),
        (_l()(), i0.ɵeld(30, 0, null, null, 3, 'label', [['class', 'lbl-sm']], null, null, null, null, null)),
        (_l()(), i0.ɵted(-1, null, ['\n				'])), (_l()(), i0.ɵeld(32, 0, null, null, 0, 'input', [['class', 'inp-check'], ['name', 'terms'], ['type',
                'checkbox']], null, null, null, null, null)), (_l()(), i0.ɵted(-1, null, ['\n				I confirm that the above information is correct and is displayed on my license disc as the above inofrmation.\n			'])),
        (_l()(), i0.ɵted(-1, null, ['\n\n		'])), (_l()(), i0.ɵted(-1, null, ['\n\n\n\n		'])), (_l()(), i0.ɵeld(36, 0, null, null, 3, 'button', [['class', 'btn btn-primary btn-lbl green'], ['type', 'button']], null, [[null, 'click']], function (_v, en, $event) {
            var ad = true;
            if (('click' === en)) {
                var pd_0 = (i0.ɵnov(_v, 37).onClick() !== false);
                ad = (pd_0 && ad);
            }
            return ad;
        }, null, null)), i0.ɵdid(37, 16384, null, 0, i1.RouterLink, [i1.Router, i1.ActivatedRoute, [8, null], i0.Renderer2, i0.ElementRef], { routerLink: [0, 'routerLink'] }, null), i0.ɵpad(38, 1), (_l()(), i0.ɵted(-1, null, ['Submit'])), (_l()(), i0.ɵted(-1, null, ['\n	'])), (_l()(),
            i0.ɵted(-1, null, ['\n'])), (_l()(), i0.ɵted(-1, null, ['\n\n']))], function (_ck, _v) {
        var currVal_3 = _ck(_v, 38, 0, '/main/thankyou');
        _ck(_v, 37, 0, currVal_3);
    }, function (_ck, _v) {
        var _co = _v.component;
        var currVal_0 = ((_co.discData == null) ? null : _co.discData.engineNo);
        _ck(_v, 15, 0, currVal_0);
        var currVal_1 = ((_co.discData == null) ? null : _co.discData.vin);
        _ck(_v, 20, 0, currVal_1);
        var currVal_2 = ((_co.discData == null) ? null : _co.discData.vehRegNo);
        _ck(_v, 25, 0, currVal_2);
    });
}
exports.View_DetailComponent_0 = View_DetailComponent_0;
function View_DetailComponent_Host_0(_l) {
    return i0.ɵvid(0, [(_l()(), i0.ɵeld(0, 0, null, null, 1, 'app-detail', [], null, null, null, View_DetailComponent_0, exports.RenderType_DetailComponent)), i0.ɵdid(1, 114688, null, 0, i2.DetailComponent, [i0.Injector], null, null)], function (_ck, _v) {
        _ck(_v, 1, 0);
    }, null);
}
exports.View_DetailComponent_Host_0 = View_DetailComponent_Host_0;
exports.DetailComponentNgFactory = i0.ɵccf('app-detail', i2.DetailComponent, View_DetailComponent_Host_0, {}, {}, []);



/***/ }),
/* 37 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/**
 * @fileoverview This file is generated by the Angular template compiler.
 * Do not edit.
 * @suppress {suspiciousCode,uselessCode,missingProperties,missingOverride}
 */
/* tslint:disable */

Object.defineProperty(exports, "__esModule", { value: true });
var i0 = __webpack_require__(0);
var i1 = __webpack_require__(25);
var i2 = __webpack_require__(2);
var styles_HomePageComponent = [];
exports.RenderType_HomePageComponent = i0.ɵcrt({ encapsulation: 2,
    styles: styles_HomePageComponent, data: {} });
function View_HomePageComponent_1(_l) {
    return i0.ɵvid(0, [(_l()(), i0.ɵeld(0, 0, null, null, 22, 'div', [['class',
                'loader']], null, null, null, null, null)),
        (_l()(), i0.ɵted(-1, null, ['\n	'])), (_l()(), i0.ɵeld(2, 0, null, null, 0, 'div', [['class', 'wrap']], null, null, null, null, null)), (_l()(), i0.ɵted(-1, null, ['\n	'])),
        (_l()(), i0.ɵeld(4, 0, null, null, 17, 'div', [['id', 'cssload-loader']], null, null, null, null, null)),
        (_l()(), i0.ɵted(-1, null, ['\n		'])), (_l()(), i0.ɵeld(6, 0, null, null, 0, 'div', [['class', 'cssload-dot']], null, null, null, null, null)), (_l()(), i0.ɵted(-1, null, ['\n		'])), (_l()(), i0.ɵeld(8, 0, null, null, 0, 'div', [['class',
                'cssload-dot']], null, null, null, null, null)), (_l()(), i0.ɵted(-1, null, ['\n		'])), (_l()(), i0.ɵeld(10, 0, null, null, 0, 'div', [['class', 'cssload-dot']], null, null, null, null, null)), (_l()(), i0.ɵted(-1, null, ['\n		'])), (_l()(), i0.ɵeld(12, 0, null, null, 0, 'div', [['class', 'cssload-dot']], null, null, null, null, null)), (_l()(), i0.ɵted(-1, null, ['\n		'])),
        (_l()(), i0.ɵeld(14, 0, null, null, 0, 'div', [['class', 'cssload-dot']], null, null, null, null, null)),
        (_l()(), i0.ɵted(-1, null, ['\n		'])), (_l()(), i0.ɵeld(16, 0, null, null, 0, 'div', [['class', 'cssload-dot']], null, null, null, null, null)), (_l()(), i0.ɵted(-1, null, ['\n		'])), (_l()(), i0.ɵeld(18, 0, null, null, 0, 'div', [['class',
                'cssload-dot']], null, null, null, null, null)), (_l()(), i0.ɵted(-1, null, ['\n		'])), (_l()(), i0.ɵeld(20, 0, null, null, 0, 'div', [['class', 'cssload-dot']], null, null, null, null, null)), (_l()(), i0.ɵted(-1, null, ['\n	'])), (_l()(), i0.ɵted(-1, null, ['\n']))], null, null);
}
function View_HomePageComponent_0(_l) {
    return i0.ɵvid(0, [(_l()(), i0.ɵeld(0, 0, null, null, 32, 'div', [['class',
                'container']], null, null, null, null, null)),
        (_l()(), i0.ɵted(-1, null, ['\n	'])), (_l()(), i0.ɵeld(2, 0, null, null, 1, 'div', [['class', 'message-block']], null, null, null, null, null)), (_l()(), i0.ɵted(-1, null, ['\n		Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s.\n	'])),
        (_l()(), i0.ɵted(-1, null, ['\n\n	'])), (_l()(), i0.ɵeld(5, 0, null, null, 26, 'div', [['class', 'btn-container text-center']], null, null, null, null, null)), (_l()(), i0.ɵted(-1, null, ['\n		'])), (_l()(), i0.ɵeld(7, 0, null, null, 11, 'div', [['class', 'file-upload']], null, null, null, null, null)), (_l()(), i0.ɵted(-1, null, ['\n			'])),
        (_l()(), i0.ɵeld(9, 0, null, null, 8, 'label', [['class', 'btn-lbl file-upload__label'],
            ['for', 'file1']], null, null, null, null, null)), (_l()(), i0.ɵted(-1, null, ['\n				'])), (_l()(), i0.ɵeld(11, 0, null, null, 0, 'input', [['accept', 'image/*'], ['class', 'inputfile form-control file-upload__input'],
            ['id', 'file'], ['name', 'file1'], ['type', 'file']], null, [[null,
                'change']], function (_v, en, $event) {
            var ad = true;
            var _co = _v.component;
            if (('change' === en)) {
                var pd_0 = (_co.onFileChange($event) !== false);
                ad = (pd_0 && ad);
            }
            return ad;
        }, null, null)), (_l()(), i0.ɵted(-1, null, ['Upload '])),
        (_l()(), i0.ɵeld(13, 0, null, null, 0, 'br', [], null, null, null, null, null)), (_l()(), i0.ɵted(-1, null, [' '])), (_l()(), i0.ɵeld(15, 0, null, null, 1, 'span', [], null, null, null, null, null)), (_l()(), i0.ɵted(-1, null, ['your license disc'])),
        (_l()(), i0.ɵted(-1, null, ['\n			'])), (_l()(), i0.ɵted(-1, null, ['\n	    '])), (_l()(), i0.ɵted(-1, null, ['\n	\n		'])), (_l()(), i0.ɵeld(20, 0, null, null, 10, 'div', [['class', 'file-upload']], null, null, null, null, null)), (_l()(), i0.ɵted(-1, null, ['\n		'])), (_l()(), i0.ɵeld(22, 0, null, null, 7, 'label', [['class', 'btn-lbl file-upload__label'], ['for', 'file2']], null, null, null, null, null)), (_l()(), i0.ɵted(-1, null, ['\n			'])), (_l()(), i0.ɵeld(24, 0, null, null, 0, 'input', [['accept', 'image/*'], ['capture', 'camera'], ['class', 'inputfile form-control file-upload__input'],
            ['id', 'capture'], ['name', 'file2'], ['type', 'file']], null, [[null,
                'change']], function (_v, en, $event) {
            var ad = true;
            var _co = _v.component;
            if (('change' === en)) {
                var pd_0 = (_co.onFileChange($event) !== false);
                ad = (pd_0 && ad);
            }
            return ad;
        }, null, null)), (_l()(), i0.ɵted(-1, null, ['Capture '])),
        (_l()(), i0.ɵeld(26, 0, null, null, 0, 'br', [], null, null, null, null, null)), (_l()(), i0.ɵted(-1, null, [' '])), (_l()(), i0.ɵeld(28, 0, null, null, 1, 'span', [], null, null, null, null, null)), (_l()(), i0.ɵted(-1, null, ['your license disc'])),
        (_l()(), i0.ɵted(-1, null, ['\n		'])), (_l()(), i0.ɵted(-1, null, ['	\n	'])), (_l()(), i0.ɵted(-1, null, ['\n'])), (_l()(), i0.ɵted(-1, null, ['\n\n'])), (_l()(), i0.ɵand(16777216, null, null, 1, null, View_HomePageComponent_1)), i0.ɵdid(35, 16384, null, 0, i2.NgIf, [i0.ViewContainerRef, i0.TemplateRef], { ngIf: [0, 'ngIf'] }, null),
        (_l()(), i0.ɵted(-1, null, ['\n']))], function (_ck, _v) {
        var _co = _v.component;
        var currVal_0 = _co.showLoader;
        _ck(_v, 35, 0, currVal_0);
    }, null);
}
exports.View_HomePageComponent_0 = View_HomePageComponent_0;
function View_HomePageComponent_Host_0(_l) {
    return i0.ɵvid(0, [(_l()(), i0.ɵeld(0, 0, null, null, 1, 'app-home-page', [], null, null, null, View_HomePageComponent_0, exports.RenderType_HomePageComponent)), i0.ɵdid(1, 114688, null, 0, i1.HomePageComponent, [i0.Injector], null, null)], function (_ck, _v) {
        _ck(_v, 1, 0);
    }, null);
}
exports.View_HomePageComponent_Host_0 = View_HomePageComponent_Host_0;
exports.HomePageComponentNgFactory = i0.ɵccf('app-home-page', i1.HomePageComponent, View_HomePageComponent_Host_0, {}, {}, []);



/***/ }),
/* 38 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/**
 * @fileoverview This file is generated by the Angular template compiler.
 * Do not edit.
 * @suppress {suspiciousCode,uselessCode,missingProperties,missingOverride}
 */
/* tslint:disable */

Object.defineProperty(exports, "__esModule", { value: true });
var i0 = __webpack_require__(0);
var i1 = __webpack_require__(2);
var i2 = __webpack_require__(1);
var i3 = __webpack_require__(26);
var styles_MainComponent = [];
exports.RenderType_MainComponent = i0.ɵcrt({ encapsulation: 2,
    styles: styles_MainComponent, data: {} });
function View_MainComponent_1(_l) {
    return i0.ɵvid(0, [(_l()(), i0.ɵeld(0, 0, null, null, 1, 'a', [['class',
                'back'], ['href', 'javascript:;'], ['title', 'back']], null, [[null,
                'click']], function (_v, en, $event) {
            var ad = true;
            var _co = _v.component;
            if (('click' === en)) {
                var pd_0 = (_co.goBack() !== false);
                ad = (pd_0 && ad);
            }
            return ad;
        }, null, null)), (_l()(), i0.ɵted(-1, null, ['Back']))], null, null);
}
function View_MainComponent_0(_l) {
    return i0.ɵvid(0, [(_l()(), i0.ɵted(-1, null, ['\n'])), (_l()(), i0.ɵeld(1, 0, null, null, 17, 'header', [['class', 'header'], ['id', 'header']], null, null, null, null, null)), (_l()(),
            i0.ɵted(-1, null, ['\n	'])), (_l()(), i0.ɵeld(3, 0, null, null, 14, 'nav', [['class', 'navbar navbar-default']], null, null, null, null, null)), (_l()(), i0.ɵted(-1, null, ['\n      '])),
        (_l()(), i0.ɵeld(5, 0, null, null, 11, 'div', [['class', 'container']], null, null, null, null, null)),
        (_l()(), i0.ɵted(-1, null, ['\n        '])), (_l()(), i0.ɵeld(7, 0, null, null, 8, 'div', [['class', 'navbar-header']], null, null, null, null, null)), (_l()(), i0.ɵted(-1, null, ['\n          '])), (_l()(), i0.ɵand(16777216, null, null, 1, null, View_MainComponent_1)), i0.ɵdid(10, 16384, null, 0, i1.NgIf, [i0.ViewContainerRef, i0.TemplateRef], { ngIf: [0, 'ngIf'] }, null),
        (_l()(), i0.ɵted(-1, null, ['\n          '])), (_l()(), i0.ɵeld(12, 0, null, null, 2, 'a', [['class', 'navbar-brand'], ['href', 'javascript:;']], null, null, null, null, null)), (_l()(), i0.ɵeld(13, 0, null, null, 1, 'h1', [['class', 'main-logo']], null, null, null, null, null)), (_l()(), i0.ɵeld(14, 0, null, null, 0, 'img', [['alt', ''], ['src', 'assets/images/logo.png']], null, null, null, null, null)),
        (_l()(), i0.ɵted(-1, null, ['\n        '])), (_l()(), i0.ɵted(-1, null, ['\n      '])), (_l()(), i0.ɵted(-1, null, ['\n    '])), (_l()(), i0.ɵted(-1, null, ['\n'])), (_l()(), i0.ɵted(-1, null, ['\n'])), (_l()(),
            i0.ɵted(-1, null, ['\n\n'])), (_l()(), i0.ɵted(-1, null, ['\n'])),
        (_l()(), i0.ɵeld(22, 0, null, null, 4, 'div', [['class', 'content']], null, null, null, null, null)),
        (_l()(), i0.ɵted(-1, null, ['\n  	'])), (_l()(), i0.ɵeld(24, 16777216, null, null, 1, 'router-outlet', [], null, null, null, null, null)), i0.ɵdid(25, 212992, null, 0, i2.RouterOutlet, [i2.ChildrenOutletContexts, i0.ViewContainerRef, i0.ComponentFactoryResolver,
            [8, null], i0.ChangeDetectorRef], null, null),
        (_l()(), i0.ɵted(-1, null, ['\n'])), (_l()(), i0.ɵted(-1, null, ['\n'])),
        (_l()(), i0.ɵted(-1, null, ['\n\n'])), (_l()(), i0.ɵted(-1, null, ['\n'])), (_l()(), i0.ɵeld(30, 0, null, null, 14, 'footer', [['class',
                'footer'], ['id', 'footer']], null, null, null, null, null)), (_l()(), i0.ɵted(-1, null, ['\n	'])), (_l()(), i0.ɵeld(32, 0, null, null, 11, 'div', [['class', 'container']], null, null, null, null, null)), (_l()(), i0.ɵted(-1, null, ['\n		  '])), (_l()(), i0.ɵeld(34, 0, null, null, 8, 'div', [['class', 'detail']], null, null, null, null, null)), (_l()(), i0.ɵted(-1, null, ['\n          '])), (_l()(),
            i0.ɵeld(36, 0, null, null, 2, 'div', [['class', 'left']], null, null, null, null, null)), (_l()(), i0.ɵeld(37, 0, null, null, 1, 'p', [], null, null, null, null, null)), (_l()(), i0.ɵted(-1, null, ['MotorHappy (Pty) Ltd, is an authorised Financial Services Provider FSP 46123.Reg No.: 2014/020352/07 '])),
        (_l()(), i0.ɵted(-1, null, ['\n          '])), (_l()(), i0.ɵeld(40, 0, null, null, 1, 'div', [['class', 'ft-logo']], null, null, null, null, null)), (_l()(), i0.ɵeld(41, 0, null, null, 0, 'img', [['alt', ''], ['src', 'assets/images/footer-logo.png']], null, null, null, null, null)),
        (_l()(), i0.ɵted(-1, null, ['\n      '])), (_l()(), i0.ɵted(-1, null, ['\n	'])), (_l()(), i0.ɵted(-1, null, ['\n'])), (_l()(), i0.ɵted(-1, null, ['\n'])), (_l()(), i0.ɵted(-1, null, ['\n\n']))], function (_ck, _v) {
        var _co = _v.component;
        var currVal_0 = _co.checkBack();
        _ck(_v, 10, 0, currVal_0);
        _ck(_v, 25, 0);
    }, null);
}
exports.View_MainComponent_0 = View_MainComponent_0;
function View_MainComponent_Host_0(_l) {
    return i0.ɵvid(0, [(_l()(), i0.ɵeld(0, 0, null, null, 1, 'app-main', [], null, null, null, View_MainComponent_0, exports.RenderType_MainComponent)),
        i0.ɵdid(1, 114688, null, 0, i3.MainComponent, [i0.Injector], null, null)], function (_ck, _v) {
        _ck(_v, 1, 0);
    }, null);
}
exports.View_MainComponent_Host_0 = View_MainComponent_Host_0;
exports.MainComponentNgFactory = i0.ɵccf('app-main', i3.MainComponent, View_MainComponent_Host_0, {}, {}, []);



/***/ }),
/* 39 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/**
 * @fileoverview This file is generated by the Angular template compiler.
 * Do not edit.
 * @suppress {suspiciousCode,uselessCode,missingProperties,missingOverride}
 */
/* tslint:disable */

Object.defineProperty(exports, "__esModule", { value: true });
var i0 = __webpack_require__(0);
var i1 = __webpack_require__(27);
var styles_ThankyouComponent = [];
exports.RenderType_ThankyouComponent = i0.ɵcrt({ encapsulation: 2,
    styles: styles_ThankyouComponent, data: {} });
function View_ThankyouComponent_0(_l) {
    return i0.ɵvid(0, [(_l()(), i0.ɵeld(0, 0, null, null, 7, 'div', [['class',
                'container']], null, null, null, null, null)),
        (_l()(), i0.ɵted(-1, null, ['\n	'])), (_l()(), i0.ɵeld(2, 0, null, null, 4, 'div', [['class', 'message-block thankyou']], null, null, null, null, null)), (_l()(), i0.ɵted(-1, null, ['\n	'])), (_l()(), i0.ɵeld(4, 0, null, null, 1, 'h3', [], null, null, null, null, null)), (_l()(), i0.ɵted(-1, null, ['Thank you for submitting your license disc information, we will update your details!'])),
        (_l()(), i0.ɵted(-1, null, ['\n	'])), (_l()(), i0.ɵted(-1, null, ['\n']))], null, null);
}
exports.View_ThankyouComponent_0 = View_ThankyouComponent_0;
function View_ThankyouComponent_Host_0(_l) {
    return i0.ɵvid(0, [(_l()(), i0.ɵeld(0, 0, null, null, 1, 'app-thankyou', [], null, null, null, View_ThankyouComponent_0, exports.RenderType_ThankyouComponent)), i0.ɵdid(1, 114688, null, 0, i1.ThankyouComponent, [i0.Injector], null, null)], function (_ck, _v) {
        _ck(_v, 1, 0);
    }, null);
}
exports.View_ThankyouComponent_Host_0 = View_ThankyouComponent_Host_0;
exports.ThankyouComponentNgFactory = i0.ɵccf('app-thankyou', i1.ThankyouComponent, View_ThankyouComponent_Host_0, {}, {}, []);



/***/ }),
/* 40 */
/***/ (function(module, exports) {

module.exports = require("@angular/animations/browser");

/***/ }),
/* 41 */
/***/ (function(module, exports) {

module.exports = require("rxjs/Observable");

/***/ }),
/* 42 */
/***/ (function(module, exports) {

module.exports = require("express");

/***/ }),
/* 43 */
/***/ (function(module, exports) {

module.exports = require("fs");

/***/ }),
/* 44 */
/***/ (function(module, exports) {

module.exports = require("path");

/***/ }),
/* 45 */
/***/ (function(module, exports) {

module.exports = require("reflect-metadata");

/***/ }),
/* 46 */
/***/ (function(module, exports) {

module.exports = require("zone.js/dist/zone-node");

/***/ }),
/* 47 */
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./app/app.browser.module.ngfactory": 50,
	"./app/app.component.ngfactory": 14,
	"./app/app.module.ngfactory": 52,
	"./app/app.server.module.ngfactory": 32,
	"./app/common/commonComponent.ngfactory": 53,
	"./app/main/camera/camera.component.ngfactory": 35,
	"./app/main/detail/detail.component.ngfactory": 36,
	"./app/main/home-page/home-page.component.ngfactory": 37,
	"./app/main/main.module.ngfactory": 55,
	"./app/main/main/main.component.ngfactory": 38,
	"./app/main/thankyou/thankyou.component.ngfactory": 39
};
function webpackContext(req) {
	return __webpack_require__(webpackContextResolve(req));
};
function webpackContextResolve(req) {
	var id = map[req];
	if(!(id + 1)) // check for number
		throw new Error("Cannot find module '" + req + "'.");
	return id;
};
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = 47;


/***/ }),
/* 48 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/**
 * @fileoverview This file is generated by the Angular template compiler.
 * Do not edit.
 * @suppress {suspiciousCode,uselessCode,missingProperties,missingOverride}
 */
/* tslint:disable */

Object.defineProperty(exports, "__esModule", { value: true });
var i0 = __webpack_require__(0);
var i1 = __webpack_require__(2);
var i2 = __webpack_require__(10);
var i3 = __webpack_require__(3);
var styles_ToastComponent = [];
exports.RenderType_ToastComponent = i0.ɵcrt({ encapsulation: 2,
    styles: styles_ToastComponent, data: {} });
function View_ToastComponent_1(_l) {
    return i0.ɵvid(0, [(_l()(), i0.ɵeld(0, 16777216, [[1, 3], ['componentBody', 1]], null, 0, 'div', [], null, null, null, null, null)), (_l()(), i0.ɵand(0, null, null, 0))], null, null);
}
function View_ToastComponent_2(_l) {
    return i0.ɵvid(0, [(_l()(), i0.ɵeld(0, 0, null, null, 0, 'div', [], [[8, 'innerHTML', 1]], null, null, null, null))], null, function (_ck, _v) {
        var _co = _v.component;
        var currVal_0 = _co.toast.body;
        _ck(_v, 0, 0, currVal_0);
    });
}
function View_ToastComponent_3(_l) {
    return i0.ɵvid(0, [(_l()(), i0.ɵeld(0, 0, null, null, 1, 'div', [], null, null, null, null, null)), (_l()(),
            i0.ɵted(1, null, ['', '']))], null, function (_ck, _v) {
        var _co = _v.component;
        var currVal_0 = _co.toast.body;
        _ck(_v, 1, 0, currVal_0);
    });
}
function View_ToastComponent_4(_l) {
    return i0.ɵvid(0, [(_l()(), i0.ɵeld(0, 0, null, null, 1, 'div', [['class',
                'toast-close-button']], [[8, 'innerHTML', 1]], [[null, 'click']], function (_v, en, $event) {
            var ad = true;
            var _co = _v.component;
            if (('click' === en)) {
                var pd_0 = (_co.click($event, _co.toast) !== false);
                ad = (pd_0 && ad);
            }
            return ad;
        }, null, null)), (_l()(), i0.ɵted(-1, null, ['\n        ']))], null, function (_ck, _v) {
        var _co = _v.component;
        var currVal_0 = _co.safeCloseHtml;
        _ck(_v, 0, 0, currVal_0);
    });
}
function View_ToastComponent_0(_l) {
    return i0.ɵvid(0, [i0.ɵqud(671088640, 1, { componentBody: 0 }), (_l()(), i0.ɵted(-1, null, ['\n        '])), (_l()(), i0.ɵeld(2, 0, null, null, 1, 'i', [['class',
                'toaster-icon']], null, null, null, null, null)),
        i0.ɵdid(3, 278528, null, 0, i1.NgClass, [i0.IterableDiffers, i0.KeyValueDiffers,
            i0.ElementRef, i0.Renderer], { klass: [0, 'klass'], ngClass: [1, 'ngClass'] }, null),
        (_l()(), i0.ɵted(-1, null, ['\n        '])), (_l()(), i0.ɵeld(5, 0, null, null, 19, 'div', [['class', 'toast-content']], null, null, null, null, null)), (_l()(), i0.ɵted(-1, null, ['\n            '])), (_l()(), i0.ɵeld(7, 0, null, null, 2, 'div', [], null, null, null, null, null)),
        i0.ɵdid(8, 278528, null, 0, i1.NgClass, [i0.IterableDiffers, i0.KeyValueDiffers,
            i0.ElementRef, i0.Renderer], { ngClass: [0, 'ngClass'] }, null), (_l()(),
            i0.ɵted(9, null, ['', ''])), (_l()(), i0.ɵted(-1, null, ['\n            '])),
        (_l()(), i0.ɵeld(11, 0, null, null, 12, 'div', [], null, null, null, null, null)), i0.ɵdid(12, 278528, null, 0, i1.NgClass, [i0.IterableDiffers, i0.KeyValueDiffers, i0.ElementRef,
            i0.Renderer], { ngClass: [0, 'ngClass'] }, null), i0.ɵdid(13, 16384, null, 0, i1.NgSwitch, [], { ngSwitch: [0, 'ngSwitch'] }, null),
        (_l()(), i0.ɵted(-1, null, ['\n                '])), (_l()(), i0.ɵand(16777216, null, null, 1, null, View_ToastComponent_1)), i0.ɵdid(16, 278528, null, 0, i1.NgSwitchCase, [i0.ViewContainerRef, i0.TemplateRef,
            i1.NgSwitch], { ngSwitchCase: [0, 'ngSwitchCase'] }, null), (_l()(),
            i0.ɵted(-1, null, ['\n                '])), (_l()(), i0.ɵand(16777216, null, null, 1, null, View_ToastComponent_2)), i0.ɵdid(19, 278528, null, 0, i1.NgSwitchCase, [i0.ViewContainerRef, i0.TemplateRef,
            i1.NgSwitch], { ngSwitchCase: [0, 'ngSwitchCase'] }, null), (_l()(),
            i0.ɵted(-1, null, ['\n                '])), (_l()(), i0.ɵand(16777216, null, null, 1, null, View_ToastComponent_3)), i0.ɵdid(22, 278528, null, 0, i1.NgSwitchCase, [i0.ViewContainerRef, i0.TemplateRef,
            i1.NgSwitch], { ngSwitchCase: [0, 'ngSwitchCase'] }, null), (_l()(),
            i0.ɵted(-1, null, ['\n            '])), (_l()(), i0.ɵted(-1, null, ['\n        '])), (_l()(), i0.ɵted(-1, null, ['\n        '])), (_l()(),
            i0.ɵand(16777216, null, null, 1, null, View_ToastComponent_4)),
        i0.ɵdid(27, 16384, null, 0, i1.NgIf, [i0.ViewContainerRef, i0.TemplateRef], { ngIf: [0, 'ngIf'] }, null)], function (_ck, _v) {
        var _co = _v.component;
        var currVal_0 = 'toaster-icon';
        var currVal_1 = _co.iconClass;
        _ck(_v, 3, 0, currVal_0, currVal_1);
        var currVal_2 = ((_co.toast.toasterConfig == null) ? null : _co.toast.toasterConfig.titleClass);
        _ck(_v, 8, 0, currVal_2);
        var currVal_4 = ((_co.toast.toasterConfig == null) ? null : _co.toast.toasterConfig.messageClass);
        _ck(_v, 12, 0, currVal_4);
        var currVal_5 = _co.toast.bodyOutputType;
        _ck(_v, 13, 0, currVal_5);
        var currVal_6 = _co.bodyOutputType.Component;
        _ck(_v, 16, 0, currVal_6);
        var currVal_7 = _co.bodyOutputType.TrustedHtml;
        _ck(_v, 19, 0, currVal_7);
        var currVal_8 = _co.bodyOutputType.Default;
        _ck(_v, 22, 0, currVal_8);
        var currVal_9 = _co.toast.showCloseButton;
        _ck(_v, 27, 0, currVal_9);
    }, function (_ck, _v) {
        var _co = _v.component;
        var currVal_3 = _co.toast.title;
        _ck(_v, 9, 0, currVal_3);
    });
}
exports.View_ToastComponent_0 = View_ToastComponent_0;
function View_ToastComponent_Host_0(_l) {
    return i0.ɵvid(0, [(_l()(), i0.ɵeld(0, 0, null, null, 1, 'div', [['toastComp',
                '']], null, null, null, View_ToastComponent_0, exports.RenderType_ToastComponent)),
        i0.ɵdid(1, 4308992, null, 0, i2.ToastComponent, [i3.DomSanitizer, i0.ComponentFactoryResolver,
            i0.ChangeDetectorRef], null, null)], function (_ck, _v) {
        _ck(_v, 1, 0);
    }, null);
}
exports.View_ToastComponent_Host_0 = View_ToastComponent_Host_0;
exports.ToastComponentNgFactory = i0.ɵccf('[toastComp]', i2.ToastComponent, View_ToastComponent_Host_0, { toast: 'toast', iconClass: 'iconClass' }, { clickEvent: 'clickEvent' }, []);



/***/ }),
/* 49 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/**
 * @fileoverview This file is generated by the Angular template compiler.
 * Do not edit.
 * @suppress {suspiciousCode,uselessCode,missingProperties,missingOverride}
 */
/* tslint:disable */

Object.defineProperty(exports, "__esModule", { value: true });
var i0 = __webpack_require__(0);
var i1 = __webpack_require__(48);
var i2 = __webpack_require__(2);
var i3 = __webpack_require__(10);
var i4 = __webpack_require__(3);
var i5 = __webpack_require__(11);
var i6 = __webpack_require__(5);
var styles_ToasterContainerComponent = [];
exports.RenderType_ToasterContainerComponent = i0.ɵcrt({ encapsulation: 2,
    styles: styles_ToasterContainerComponent, data: { 'animation': [{ type: 7, name: 'toastState',
                definitions: [{ type: 0, name: 'flyRight, flyLeft, slideDown, slideUp, fade', styles: { type: 6,
                            styles: { opacity: 1, transform: 'translate(0,0)' }, offset: null }, options: undefined },
                    { type: 1, expr: 'void => flyRight', animation: [{ type: 6, styles: { opacity: 0, transform: 'translateX(100%)' },
                                offset: null }, { type: 4, styles: null, timings: '0.25s ease-in' }],
                        options: null }, { type: 1, expr: 'flyRight => void', animation: [{ type: 4,
                                styles: { type: 6, styles: { opacity: 0, transform: 'translateX(100%)' }, offset: null },
                                timings: '0.25s 10ms ease-out' }], options: null }, { type: 1, expr: 'void => flyLeft',
                        animation: [{ type: 6, styles: { opacity: 0, transform: 'translateX(-100%)' },
                                offset: null }, { type: 4, styles: null, timings: '0.25s ease-in' }],
                        options: null }, { type: 1, expr: 'flyLeft => void', animation: [{ type: 4,
                                styles: { type: 6, styles: { opacity: 0, transform: 'translateX(-100%)' }, offset: null },
                                timings: '0.25s 10ms ease-out' }], options: null }, { type: 1, expr: 'void => slideDown',
                        animation: [{ type: 6, styles: { opacity: 0, transform: 'translateY(-200%)' },
                                offset: null }, { type: 4, styles: null, timings: '0.3s ease-in' }],
                        options: null }, { type: 1, expr: 'slideDown => void', animation: [{ type: 4,
                                styles: { type: 6, styles: { opacity: 0, transform: 'translateY(200%)' }, offset: null },
                                timings: '0.3s 10ms ease-out' }], options: null }, { type: 1, expr: 'void => slideUp',
                        animation: [{ type: 6, styles: { opacity: 0, transform: 'translateY(200%)' },
                                offset: null }, { type: 4, styles: null, timings: '0.3s ease-in' }],
                        options: null }, { type: 1, expr: 'slideUp => void', animation: [{ type: 4,
                                styles: { type: 6, styles: { opacity: 0, transform: 'translateY(-200%)' }, offset: null },
                                timings: '0.3s 10ms ease-out' }], options: null }, { type: 1, expr: 'void => fade',
                        animation: [{ type: 6, styles: { opacity: 0 }, offset: null }, { type: 4,
                                styles: null, timings: '0.3s ease-in' }], options: null },
                    { type: 1, expr: 'fade => void', animation: [{ type: 4, styles: { type: 6, styles: { opacity: 0 },
                                    offset: null }, timings: '0.3s 10ms ease-out' }], options: null }],
                options: {} }] } });
function View_ToasterContainerComponent_1(_l) {
    return i0.ɵvid(0, [(_l()(), i0.ɵeld(0, 0, null, null, 3, 'div', [['class',
                'toast'], ['toastComp', '']], [[24, '@toastState', 0]], [[null, 'click'], [null,
                'clickEvent'], [null, 'mouseover'], [null, 'mouseout']], function (_v, en, $event) {
            var ad = true;
            var _co = _v.component;
            if (('click' === en)) {
                var pd_0 = (_co.click(_v.context.$implicit) !== false);
                ad = (pd_0 && ad);
            }
            if (('clickEvent' === en)) {
                var pd_1 = (_co.childClick($event) !== false);
                ad = (pd_1 && ad);
            }
            if (('mouseover' === en)) {
                var pd_2 = (_co.stopTimer(_v.context.$implicit) !== false);
                ad = (pd_2 && ad);
            }
            if (('mouseout' === en)) {
                var pd_3 = (_co.restartTimer(_v.context.$implicit) !== false);
                ad = (pd_3 && ad);
            }
            return ad;
        }, i1.View_ToastComponent_0, i1.RenderType_ToastComponent)), i0.ɵdid(1, 278528, null, 0, i2.NgClass, [i0.IterableDiffers, i0.KeyValueDiffers, i0.ElementRef, i0.Renderer], { klass: [0, 'klass'], ngClass: [1, 'ngClass'] }, null), i0.ɵdid(2, 4308992, null, 0, i3.ToastComponent, [i4.DomSanitizer, i0.ComponentFactoryResolver, i0.ChangeDetectorRef], { toast: [0, 'toast'], iconClass: [1, 'iconClass'] }, { clickEvent: 'clickEvent' }), (_l()(),
            i0.ɵted(-1, null, ['\n            ']))], function (_ck, _v) {
        var _co = _v.component;
        var currVal_1 = 'toast';
        var currVal_2 = _co.toasterconfig.typeClasses[_v.context.$implicit.type];
        _ck(_v, 1, 0, currVal_1, currVal_2);
        var currVal_3 = _v.context.$implicit;
        var currVal_4 = _co.toasterconfig.iconClasses[_v.context.$implicit.type];
        _ck(_v, 2, 0, currVal_3, currVal_4);
    }, function (_ck, _v) {
        var _co = _v.component;
        var currVal_0 = _co.toasterconfig.animation;
        _ck(_v, 0, 0, currVal_0);
    });
}
function View_ToasterContainerComponent_0(_l) {
    return i0.ɵvid(0, [(_l()(), i0.ɵted(-1, null, ['\n        '])), (_l()(), i0.ɵeld(1, 0, null, null, 6, 'div', [['id', 'toast-container']], null, null, null, null, null)), i0.ɵdid(2, 278528, null, 0, i2.NgClass, [i0.IterableDiffers, i0.KeyValueDiffers, i0.ElementRef, i0.Renderer], { ngClass: [0, 'ngClass'] }, null), i0.ɵpad(3, 1), (_l()(), i0.ɵted(-1, null, ['\n            '])), (_l()(), i0.ɵand(16777216, null, null, 1, null, View_ToasterContainerComponent_1)), i0.ɵdid(6, 802816, null, 0, i2.NgForOf, [i0.ViewContainerRef, i0.TemplateRef, i0.IterableDiffers], { ngForOf: [0,
                'ngForOf'] }, null), (_l()(), i0.ɵted(-1, null, ['\n        '])),
        (_l()(), i0.ɵted(-1, null, ['\n        ']))], function (_ck, _v) {
        var _co = _v.component;
        var currVal_0 = _ck(_v, 3, 0, _co.toasterconfig.positionClass);
        _ck(_v, 2, 0, currVal_0);
        var currVal_1 = _co.toasts;
        _ck(_v, 6, 0, currVal_1);
    }, null);
}
exports.View_ToasterContainerComponent_0 = View_ToasterContainerComponent_0;
function View_ToasterContainerComponent_Host_0(_l) {
    return i0.ɵvid(0, [(_l()(), i0.ɵeld(0, 0, null, null, 1, 'toaster-container', [], null, null, null, View_ToasterContainerComponent_0, exports.RenderType_ToasterContainerComponent)), i0.ɵdid(1, 245760, null, 0, i5.ToasterContainerComponent, [i6.ToasterService, i0.ChangeDetectorRef, i0.NgZone], null, null)], function (_ck, _v) {
        _ck(_v, 1, 0);
    }, null);
}
exports.View_ToasterContainerComponent_Host_0 = View_ToasterContainerComponent_Host_0;
exports.ToasterContainerComponentNgFactory = i0.ɵccf('toaster-container', i5.ToasterContainerComponent, View_ToasterContainerComponent_Host_0, { toasterconfig: 'toasterconfig' }, {}, []);



/***/ }),
/* 50 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/**
 * @fileoverview This file is generated by the Angular template compiler.
 * Do not edit.
 * @suppress {suspiciousCode,uselessCode,missingProperties,missingOverride}
 */
/* tslint:disable */

Object.defineProperty(exports, "__esModule", { value: true });
var i0 = __webpack_require__(0);
var i1 = __webpack_require__(56);
var i2 = __webpack_require__(7);
var i3 = __webpack_require__(14);
var i4 = __webpack_require__(2);
var i5 = __webpack_require__(3);
var i6 = __webpack_require__(40);
var i7 = __webpack_require__(18);
var i8 = __webpack_require__(28);
var i9 = __webpack_require__(4);
var i10 = __webpack_require__(30);
var i11 = __webpack_require__(31);
var i12 = __webpack_require__(5);
var i13 = __webpack_require__(17);
var i14 = __webpack_require__(1);
var i15 = __webpack_require__(15);
var i16 = __webpack_require__(13);
var i17 = __webpack_require__(9);
var i18 = __webpack_require__(29);
var i19 = __webpack_require__(12);
var i20 = __webpack_require__(8);
exports.AppBrowserModuleNgFactory = i0.ɵcmf(i1.AppBrowserModule, [i2.AppComponent], function (_l) {
    return i0.ɵmod([i0.ɵmpd(512, i0.ComponentFactoryResolver, i0.ɵCodegenComponentFactoryResolver, [[8, [i3.AppComponentNgFactory]], [3, i0.ComponentFactoryResolver], i0.NgModuleRef]),
        i0.ɵmpd(5120, i0.LOCALE_ID, i0.ɵm, [[3, i0.LOCALE_ID]]), i0.ɵmpd(4608, i4.NgLocalization, i4.NgLocaleLocalization, [i0.LOCALE_ID]), i0.ɵmpd(5120, i0.IterableDiffers, i0.ɵk, []), i0.ɵmpd(5120, i0.KeyValueDiffers, i0.ɵl, []),
        i0.ɵmpd(4608, i5.DomSanitizer, i5.ɵe, [i4.DOCUMENT]), i0.ɵmpd(6144, i0.Sanitizer, null, [i5.DomSanitizer]), i0.ɵmpd(4608, i5.HAMMER_GESTURE_CONFIG, i5.HammerGestureConfig, []), i0.ɵmpd(5120, i5.EVENT_MANAGER_PLUGINS, function (p0_0, p1_0, p2_0, p2_1) {
            return [new i5.ɵDomEventsPlugin(p0_0), new i5.ɵKeyEventsPlugin(p1_0),
                new i5.ɵHammerGesturesPlugin(p2_0, p2_1)];
        }, [i4.DOCUMENT, i4.DOCUMENT, i4.DOCUMENT, i5.HAMMER_GESTURE_CONFIG]), i0.ɵmpd(4608, i5.EventManager, i5.EventManager, [i5.EVENT_MANAGER_PLUGINS, i0.NgZone]),
        i0.ɵmpd(135680, i5.ɵDomSharedStylesHost, i5.ɵDomSharedStylesHost, [i4.DOCUMENT]),
        i0.ɵmpd(4608, i5.ɵDomRendererFactory2, i5.ɵDomRendererFactory2, [i5.EventManager,
            i5.ɵDomSharedStylesHost]), i0.ɵmpd(5120, i6.AnimationDriver, i7.ɵc, []),
        i0.ɵmpd(5120, i6.ɵAnimationStyleNormalizer, i7.ɵd, []), i0.ɵmpd(4608, i6.ɵAnimationEngine, i7.ɵb, [i6.AnimationDriver, i6.ɵAnimationStyleNormalizer]),
        i0.ɵmpd(5120, i0.RendererFactory2, i7.ɵe, [i5.ɵDomRendererFactory2, i6.ɵAnimationEngine,
            i0.NgZone]), i0.ɵmpd(6144, i5.ɵSharedStylesHost, null, [i5.ɵDomSharedStylesHost]),
        i0.ɵmpd(4608, i0.Testability, i0.Testability, [i0.NgZone]), i0.ɵmpd(4608, i5.Meta, i5.Meta, [i4.DOCUMENT]), i0.ɵmpd(4608, i5.Title, i5.Title, [i4.DOCUMENT]),
        i0.ɵmpd(4608, i8.AnimationBuilder, i7.ɵBrowserAnimationBuilder, [i0.RendererFactory2,
            i5.DOCUMENT]), i0.ɵmpd(4608, i9.BrowserXhr, i9.BrowserXhr, []),
        i0.ɵmpd(4608, i9.ResponseOptions, i9.BaseResponseOptions, []), i0.ɵmpd(5120, i9.XSRFStrategy, i9.ɵb, []), i0.ɵmpd(4608, i9.XHRBackend, i9.XHRBackend, [i9.BrowserXhr, i9.ResponseOptions, i9.XSRFStrategy]), i0.ɵmpd(4608, i9.RequestOptions, i9.BaseRequestOptions, []), i0.ɵmpd(5120, i9.Http, i9.ɵc, [i9.XHRBackend,
            i9.RequestOptions]), i0.ɵmpd(4608, i10.CoolLocalStorage, i10.CoolLocalStorage, [i0.PLATFORM_ID]), i0.ɵmpd(4608, i11.CoolSessionStorage, i11.CoolSessionStorage, [i0.PLATFORM_ID]), i0.ɵmpd(4608, i12.ToasterService, i12.ToasterService, []), i0.ɵmpd(4608, i13.ɵi, i13.ɵi, []), i0.ɵmpd(5120, i14.ActivatedRoute, i14.ɵf, [i14.Router]), i0.ɵmpd(4608, i14.NoPreloading, i14.NoPreloading, []), i0.ɵmpd(6144, i14.PreloadingStrategy, null, [i14.NoPreloading]), i0.ɵmpd(135680, i14.RouterPreloader, i14.RouterPreloader, [i14.Router, i0.NgModuleFactoryLoader, i0.Compiler,
            i0.Injector, i14.PreloadingStrategy]), i0.ɵmpd(4608, i14.PreloadAllModules, i14.PreloadAllModules, []), i0.ɵmpd(5120, i14.ROUTER_INITIALIZER, i14.ɵi, [i14.ɵg]), i0.ɵmpd(5120, i0.APP_BOOTSTRAP_LISTENER, function (p0_0) {
            return [p0_0];
        }, [i14.ROUTER_INITIALIZER]), i0.ɵmpd(4608, i15.AlwaysAuthChildrenGuard, i15.AlwaysAuthChildrenGuard, [i10.CoolLocalStorage, i14.Router]), i0.ɵmpd(4608, i15.CanLoginActivate, i15.CanLoginActivate, [i10.CoolLocalStorage, i14.Router]), i0.ɵmpd(4608, i16.CookieService, i16.CookieService, [i5.DOCUMENT]), i0.ɵmpd(4608, i17.CommonService, i17.CommonService, [i0.Injector, i10.CoolLocalStorage, i9.Http]), i0.ɵmpd(512, i4.CommonModule, i4.CommonModule, []), i0.ɵmpd(1024, i0.ErrorHandler, i5.ɵa, []), i0.ɵmpd(1024, i0.NgProbeToken, function () {
            return [i14.ɵb()];
        }, []), i0.ɵmpd(256, i0.APP_ID, 'vin-frontend-ng4', []),
        i0.ɵmpd(2048, i5.ɵTRANSITION_ID, null, [i0.APP_ID]), i0.ɵmpd(512, i14.ɵg, i14.ɵg, [i0.Injector]), i0.ɵmpd(1024, i0.APP_INITIALIZER, function (p0_0, p0_1, p1_0, p1_1, p1_2, p2_0) {
            return [i5.ɵc(p0_0, p0_1), i5.ɵf(p1_0, p1_1, p1_2), i14.ɵh(p2_0)];
        }, [[2, i5.NgProbeToken], [2, i0.NgProbeToken], i5.ɵTRANSITION_ID, i4.DOCUMENT,
            i0.Injector, i14.ɵg]), i0.ɵmpd(512, i0.ApplicationInitStatus, i0.ApplicationInitStatus, [[2, i0.APP_INITIALIZER]]), i0.ɵmpd(131584, i0.ɵe, i0.ɵe, [i0.NgZone, i0.ɵConsole,
            i0.Injector, i0.ErrorHandler, i0.ComponentFactoryResolver, i0.ApplicationInitStatus]),
        i0.ɵmpd(2048, i0.ApplicationRef, null, [i0.ɵe]), i0.ɵmpd(512, i0.ApplicationModule, i0.ApplicationModule, [i0.ApplicationRef]), i0.ɵmpd(512, i5.BrowserModule, i5.BrowserModule, [[3, i5.BrowserModule]]), i0.ɵmpd(512, i7.BrowserAnimationsModule, i7.BrowserAnimationsModule, []), i0.ɵmpd(512, i9.HttpModule, i9.HttpModule, []), i0.ɵmpd(512, i18.CoolStorageModule, i18.CoolStorageModule, []), i0.ɵmpd(512, i19.ToasterModule, i19.ToasterModule, []),
        i0.ɵmpd(512, i13.ɵba, i13.ɵba, []), i0.ɵmpd(512, i13.FormsModule, i13.FormsModule, []), i0.ɵmpd(1024, i14.ɵa, i14.ɵd, [[3, i14.Router]]), i0.ɵmpd(512, i14.UrlSerializer, i14.DefaultUrlSerializer, []), i0.ɵmpd(512, i14.ChildrenOutletContexts, i14.ChildrenOutletContexts, []),
        i0.ɵmpd(256, i14.ROUTER_CONFIGURATION, { initialNavigation: 'enabled', useHash: false }, []), i0.ɵmpd(1024, i4.LocationStrategy, i14.ɵc, [i4.PlatformLocation,
            [2, i4.APP_BASE_HREF], i14.ROUTER_CONFIGURATION]), i0.ɵmpd(512, i4.Location, i4.Location, [i4.LocationStrategy]), i0.ɵmpd(512, i0.Compiler, i0.Compiler, []), i0.ɵmpd(512, i0.NgModuleFactoryLoader, i0.SystemJsNgModuleLoader, [i0.Compiler, [2, i0.SystemJsNgModuleLoaderConfig]]), i0.ɵmpd(1024, i14.ROUTES, function () {
            return [[{ path: 'main', loadChildren: './main/main.module#MainModule' },
                    { path: '', redirectTo: 'main', pathMatch: 'full' }, { path: '**', redirectTo: 'main',
                        pathMatch: 'full' }]];
        }, []), i0.ɵmpd(1024, i14.Router, i14.ɵe, [i0.ApplicationRef, i14.UrlSerializer,
            i14.ChildrenOutletContexts, i4.Location, i0.Injector, i0.NgModuleFactoryLoader,
            i0.Compiler, i14.ROUTES, i14.ROUTER_CONFIGURATION, [2, i14.UrlHandlingStrategy],
            [2, i14.RouteReuseStrategy]]), i0.ɵmpd(512, i14.RouterModule, i14.RouterModule, [[2, i14.ɵa], [2, i14.Router]]), i0.ɵmpd(512, i20.AppModule, i20.AppModule, []), i0.ɵmpd(512, i1.AppBrowserModule, i1.AppBrowserModule, [])]);
});



/***/ }),
/* 51 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/**
 * @fileoverview This file is generated by the Angular template compiler.
 * Do not edit.
 * @suppress {suspiciousCode,uselessCode,missingProperties,missingOverride}
 */
/* tslint:disable */

Object.defineProperty(exports, "__esModule", { value: true });
exports.styles = [''];



/***/ }),
/* 52 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/**
 * @fileoverview This file is generated by the Angular template compiler.
 * Do not edit.
 * @suppress {suspiciousCode,uselessCode,missingProperties,missingOverride}
 */
/* tslint:disable */

Object.defineProperty(exports, "__esModule", { value: true });
var i0 = __webpack_require__(0);
var i1 = __webpack_require__(8);
var i2 = __webpack_require__(7);
var i3 = __webpack_require__(14);
var i4 = __webpack_require__(4);
var i5 = __webpack_require__(30);
var i6 = __webpack_require__(31);
var i7 = __webpack_require__(2);
var i8 = __webpack_require__(5);
var i9 = __webpack_require__(17);
var i10 = __webpack_require__(3);
var i11 = __webpack_require__(1);
var i12 = __webpack_require__(15);
var i13 = __webpack_require__(13);
var i14 = __webpack_require__(9);
var i15 = __webpack_require__(29);
var i16 = __webpack_require__(12);
exports.AppModuleNgFactory = i0.ɵcmf(i1.AppModule, [i2.AppComponent], function (_l) {
    return i0.ɵmod([i0.ɵmpd(512, i0.ComponentFactoryResolver, i0.ɵCodegenComponentFactoryResolver, [[8, [i3.AppComponentNgFactory]], [3, i0.ComponentFactoryResolver], i0.NgModuleRef]),
        i0.ɵmpd(4608, i4.BrowserXhr, i4.BrowserXhr, []), i0.ɵmpd(4608, i4.ResponseOptions, i4.BaseResponseOptions, []), i0.ɵmpd(5120, i4.XSRFStrategy, i4.ɵb, []), i0.ɵmpd(4608, i4.XHRBackend, i4.XHRBackend, [i4.BrowserXhr,
            i4.ResponseOptions, i4.XSRFStrategy]), i0.ɵmpd(4608, i4.RequestOptions, i4.BaseRequestOptions, []), i0.ɵmpd(5120, i4.Http, i4.ɵc, [i4.XHRBackend,
            i4.RequestOptions]), i0.ɵmpd(4608, i5.CoolLocalStorage, i5.CoolLocalStorage, [i0.PLATFORM_ID]), i0.ɵmpd(4608, i6.CoolSessionStorage, i6.CoolSessionStorage, [i0.PLATFORM_ID]), i0.ɵmpd(5120, i0.LOCALE_ID, i0.ɵm, [[3, i0.LOCALE_ID]]),
        i0.ɵmpd(4608, i7.NgLocalization, i7.NgLocaleLocalization, [i0.LOCALE_ID]), i0.ɵmpd(4608, i8.ToasterService, i8.ToasterService, []), i0.ɵmpd(4608, i9.ɵi, i9.ɵi, []), i0.ɵmpd(5120, i0.IterableDiffers, i0.ɵk, []),
        i0.ɵmpd(5120, i0.KeyValueDiffers, i0.ɵl, []), i0.ɵmpd(4608, i10.DomSanitizer, i10.ɵe, [i7.DOCUMENT]), i0.ɵmpd(6144, i0.Sanitizer, null, [i10.DomSanitizer]),
        i0.ɵmpd(4608, i10.HAMMER_GESTURE_CONFIG, i10.HammerGestureConfig, []),
        i0.ɵmpd(5120, i10.EVENT_MANAGER_PLUGINS, function (p0_0, p1_0, p2_0, p2_1) {
            return [new i10.ɵDomEventsPlugin(p0_0), new i10.ɵKeyEventsPlugin(p1_0),
                new i10.ɵHammerGesturesPlugin(p2_0, p2_1)];
        }, [i7.DOCUMENT, i7.DOCUMENT, i7.DOCUMENT, i10.HAMMER_GESTURE_CONFIG]), i0.ɵmpd(4608, i10.EventManager, i10.EventManager, [i10.EVENT_MANAGER_PLUGINS, i0.NgZone]),
        i0.ɵmpd(135680, i10.ɵDomSharedStylesHost, i10.ɵDomSharedStylesHost, [i7.DOCUMENT]),
        i0.ɵmpd(4608, i10.ɵDomRendererFactory2, i10.ɵDomRendererFactory2, [i10.EventManager,
            i10.ɵDomSharedStylesHost]), i0.ɵmpd(6144, i0.RendererFactory2, null, [i10.ɵDomRendererFactory2]), i0.ɵmpd(6144, i10.ɵSharedStylesHost, null, [i10.ɵDomSharedStylesHost]), i0.ɵmpd(4608, i0.Testability, i0.Testability, [i0.NgZone]), i0.ɵmpd(4608, i10.Meta, i10.Meta, [i7.DOCUMENT]), i0.ɵmpd(4608, i10.Title, i10.Title, [i7.DOCUMENT]), i0.ɵmpd(5120, i11.ActivatedRoute, i11.ɵf, [i11.Router]), i0.ɵmpd(4608, i11.NoPreloading, i11.NoPreloading, []),
        i0.ɵmpd(6144, i11.PreloadingStrategy, null, [i11.NoPreloading]), i0.ɵmpd(135680, i11.RouterPreloader, i11.RouterPreloader, [i11.Router, i0.NgModuleFactoryLoader,
            i0.Compiler, i0.Injector, i11.PreloadingStrategy]), i0.ɵmpd(4608, i11.PreloadAllModules, i11.PreloadAllModules, []), i0.ɵmpd(5120, i11.ROUTER_INITIALIZER, i11.ɵi, [i11.ɵg]), i0.ɵmpd(5120, i0.APP_BOOTSTRAP_LISTENER, function (p0_0) {
            return [p0_0];
        }, [i11.ROUTER_INITIALIZER]), i0.ɵmpd(4608, i12.AlwaysAuthChildrenGuard, i12.AlwaysAuthChildrenGuard, [i5.CoolLocalStorage, i11.Router]), i0.ɵmpd(4608, i12.CanLoginActivate, i12.CanLoginActivate, [i5.CoolLocalStorage, i11.Router]), i0.ɵmpd(4608, i13.CookieService, i13.CookieService, [i10.DOCUMENT]), i0.ɵmpd(4608, i14.CommonService, i14.CommonService, [i0.Injector, i5.CoolLocalStorage, i4.Http]), i0.ɵmpd(512, i4.HttpModule, i4.HttpModule, []), i0.ɵmpd(512, i15.CoolStorageModule, i15.CoolStorageModule, []), i0.ɵmpd(512, i7.CommonModule, i7.CommonModule, []), i0.ɵmpd(512, i16.ToasterModule, i16.ToasterModule, []),
        i0.ɵmpd(512, i9.ɵba, i9.ɵba, []), i0.ɵmpd(512, i9.FormsModule, i9.FormsModule, []), i0.ɵmpd(1024, i0.ErrorHandler, i10.ɵa, []), i0.ɵmpd(1024, i0.NgProbeToken, function () {
            return [i11.ɵb()];
        }, []), i0.ɵmpd(256, i0.APP_ID, 'vin-frontend-ng4', []),
        i0.ɵmpd(2048, i10.ɵTRANSITION_ID, null, [i0.APP_ID]), i0.ɵmpd(512, i11.ɵg, i11.ɵg, [i0.Injector]), i0.ɵmpd(1024, i0.APP_INITIALIZER, function (p0_0, p0_1, p1_0, p1_1, p1_2, p2_0) {
            return [i10.ɵc(p0_0, p0_1), i10.ɵf(p1_0, p1_1, p1_2), i11.ɵh(p2_0)];
        }, [[2, i10.NgProbeToken], [2, i0.NgProbeToken], i10.ɵTRANSITION_ID, i7.DOCUMENT,
            i0.Injector, i11.ɵg]), i0.ɵmpd(512, i0.ApplicationInitStatus, i0.ApplicationInitStatus, [[2, i0.APP_INITIALIZER]]), i0.ɵmpd(131584, i0.ɵe, i0.ɵe, [i0.NgZone, i0.ɵConsole,
            i0.Injector, i0.ErrorHandler, i0.ComponentFactoryResolver, i0.ApplicationInitStatus]),
        i0.ɵmpd(2048, i0.ApplicationRef, null, [i0.ɵe]), i0.ɵmpd(512, i0.ApplicationModule, i0.ApplicationModule, [i0.ApplicationRef]), i0.ɵmpd(512, i10.BrowserModule, i10.BrowserModule, [[3, i10.BrowserModule]]), i0.ɵmpd(1024, i11.ɵa, i11.ɵd, [[3, i11.Router]]), i0.ɵmpd(512, i11.UrlSerializer, i11.DefaultUrlSerializer, []), i0.ɵmpd(512, i11.ChildrenOutletContexts, i11.ChildrenOutletContexts, []), i0.ɵmpd(256, i11.ROUTER_CONFIGURATION, { initialNavigation: 'enabled',
            useHash: false }, []), i0.ɵmpd(1024, i7.LocationStrategy, i11.ɵc, [i7.PlatformLocation, [2, i7.APP_BASE_HREF], i11.ROUTER_CONFIGURATION]),
        i0.ɵmpd(512, i7.Location, i7.Location, [i7.LocationStrategy]), i0.ɵmpd(512, i0.Compiler, i0.Compiler, []), i0.ɵmpd(512, i0.NgModuleFactoryLoader, i0.SystemJsNgModuleLoader, [i0.Compiler, [2, i0.SystemJsNgModuleLoaderConfig]]), i0.ɵmpd(1024, i11.ROUTES, function () {
            return [[{ path: 'main', loadChildren: './main/main.module#MainModule' },
                    { path: '', redirectTo: 'main', pathMatch: 'full' }, { path: '**', redirectTo: 'main',
                        pathMatch: 'full' }]];
        }, []), i0.ɵmpd(1024, i11.Router, i11.ɵe, [i0.ApplicationRef, i11.UrlSerializer,
            i11.ChildrenOutletContexts, i7.Location, i0.Injector, i0.NgModuleFactoryLoader,
            i0.Compiler, i11.ROUTES, i11.ROUTER_CONFIGURATION, [2, i11.UrlHandlingStrategy],
            [2, i11.RouteReuseStrategy]]), i0.ɵmpd(512, i11.RouterModule, i11.RouterModule, [[2, i11.ɵa], [2, i11.Router]]), i0.ɵmpd(512, i1.AppModule, i1.AppModule, [])]);
});



/***/ }),
/* 53 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/**
 * @fileoverview This file is generated by the Angular template compiler.
 * Do not edit.
 * @suppress {suspiciousCode,uselessCode,missingProperties,missingOverride}
 */
/* tslint:disable */

Object.defineProperty(exports, "__esModule", { value: true });
var i0 = __webpack_require__(0);
var i1 = __webpack_require__(6);
var styles_BaseComponent = [];
exports.RenderType_BaseComponent = i0.ɵcrt({ encapsulation: 2,
    styles: styles_BaseComponent, data: {} });
function View_BaseComponent_0(_l) {
    return i0.ɵvid(0, [(_l()(), i0.ɵeld(0, 0, null, null, 1, 'h1', [], null, null, null, null, null)), (_l()(),
            i0.ɵted(-1, null, ['Hello from ParentComponent ']))], null, null);
}
exports.View_BaseComponent_0 = View_BaseComponent_0;
function View_BaseComponent_Host_0(_l) {
    return i0.ɵvid(0, [(_l()(), i0.ɵeld(0, 0, null, null, 1, 'parent-comp', [], null, null, null, View_BaseComponent_0, exports.RenderType_BaseComponent)), i0.ɵdid(1, 49152, null, 0, i1.BaseComponent, [i0.Injector], null, null)], null, null);
}
exports.View_BaseComponent_Host_0 = View_BaseComponent_Host_0;
exports.BaseComponentNgFactory = i0.ɵccf('parent-comp', i1.BaseComponent, View_BaseComponent_Host_0, {}, {}, []);



/***/ }),
/* 54 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/**
 * @fileoverview This file is generated by the Angular template compiler.
 * Do not edit.
 * @suppress {suspiciousCode,uselessCode,missingProperties,missingOverride}
 */
/* tslint:disable */

Object.defineProperty(exports, "__esModule", { value: true });
exports.styles = [''];



/***/ }),
/* 55 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/**
 * @fileoverview This file is generated by the Angular template compiler.
 * Do not edit.
 * @suppress {suspiciousCode,uselessCode,missingProperties,missingOverride}
 */
/* tslint:disable */

Object.defineProperty(exports, "__esModule", { value: true });
var i0 = __webpack_require__(0);
var i1 = __webpack_require__(58);
var i2 = __webpack_require__(38);
var i3 = __webpack_require__(37);
var i4 = __webpack_require__(36);
var i5 = __webpack_require__(39);
var i6 = __webpack_require__(35);
var i7 = __webpack_require__(2);
var i8 = __webpack_require__(16);
var i9 = __webpack_require__(4);
var i10 = __webpack_require__(1);
var i11 = __webpack_require__(26);
var i12 = __webpack_require__(25);
var i13 = __webpack_require__(24);
var i14 = __webpack_require__(27);
var i15 = __webpack_require__(23);
exports.MainModuleNgFactory = i0.ɵcmf(i1.MainModule, [], function (_l) {
    return i0.ɵmod([i0.ɵmpd(512, i0.ComponentFactoryResolver, i0.ɵCodegenComponentFactoryResolver, [[8, [i2.MainComponentNgFactory, i3.HomePageComponentNgFactory, i4.DetailComponentNgFactory,
                    i5.ThankyouComponentNgFactory, i6.CameraComponentNgFactory]], [3, i0.ComponentFactoryResolver],
            i0.NgModuleRef]), i0.ɵmpd(4608, i7.NgLocalization, i7.NgLocaleLocalization, [i0.LOCALE_ID]), i0.ɵmpd(4608, i8.CameraService, i8.CameraService, [i9.Http]),
        i0.ɵmpd(512, i7.CommonModule, i7.CommonModule, []), i0.ɵmpd(512, i10.RouterModule, i10.RouterModule, [[2, i10.ɵa], [2, i10.Router]]), i0.ɵmpd(512, i1.MainModule, i1.MainModule, []), i0.ɵmpd(1024, i10.ROUTES, function () {
            return [[{ path: '', component: i11.MainComponent, children: [{ path: '', redirectTo: 'home',
                                pathMatch: 'full' }, { path: 'home', component: i12.HomePageComponent, pathMatch: 'full' },
                            { path: 'detail', component: i13.DetailComponent, pathMatch: 'full' }, { path: 'thankyou',
                                component: i14.ThankyouComponent, pathMatch: 'full' }, { path: 'camera',
                                component: i15.CameraComponent, pathMatch: 'full' }, { path: '**', redirectTo: 'home',
                                pathMatch: 'full' }] }, { path: '**', redirectTo: '', pathMatch: 'full' }]];
        }, [])]);
});



/***/ }),
/* 56 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(0);
var animations_1 = __webpack_require__(18);
var app_module_1 = __webpack_require__(8);
var app_component_1 = __webpack_require__(7);
var AppBrowserModule = (function () {
    function AppBrowserModule() {
    }
    return AppBrowserModule;
}());
AppBrowserModule = __decorate([
    core_1.NgModule({
        imports: [
            animations_1.BrowserAnimationsModule,
            app_module_1.AppModule
        ],
        bootstrap: [app_component_1.AppComponent]
    })
], AppBrowserModule);
exports.AppBrowserModule = AppBrowserModule;


/***/ }),
/* 57 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/// <reference types="node" />

var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(0);
var platform_server_1 = __webpack_require__(20);
var app_module_1 = __webpack_require__(8);
var animations_1 = __webpack_require__(18);
var ServerFactoryLoader = (function (_super) {
    __extends(ServerFactoryLoader, _super);
    function ServerFactoryLoader() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    ServerFactoryLoader.prototype.load = function (path) {
        return new Promise(function (resolve, reject) {
            var _a = path.split('#'), file = _a[0], className = _a[1];
            var classes = __webpack_require__(47)("./app" + file.slice(1) + '.ngfactory');
            resolve(classes[className + 'NgFactory']);
        });
    };
    return ServerFactoryLoader;
}(core_1.NgModuleFactoryLoader));
exports.ServerFactoryLoader = ServerFactoryLoader;
var AppServerModule = (function () {
    function AppServerModule() {
    }
    return AppServerModule;
}());
AppServerModule = __decorate([
    core_1.NgModule({
        imports: [
            platform_server_1.ServerModule,
            animations_1.NoopAnimationsModule,
            app_module_1.AppModule
        ],
        bootstrap: [app_module_1.AppComponent],
        providers: [
            { provide: core_1.NgModuleFactoryLoader, useClass: ServerFactoryLoader }
        ]
    })
], AppServerModule);
exports.AppServerModule = AppServerModule;


/***/ }),
/* 58 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(0);
var router_1 = __webpack_require__(1);
var common_1 = __webpack_require__(2);
var home_page_component_1 = __webpack_require__(25);
var main_component_1 = __webpack_require__(26);
var detail_component_1 = __webpack_require__(24);
var thankyou_component_1 = __webpack_require__(27);
var camera_component_1 = __webpack_require__(23);
var camera_service_1 = __webpack_require__(16);
var MainModule = (function () {
    function MainModule() {
    }
    return MainModule;
}());
MainModule = __decorate([
    core_1.NgModule({
        imports: [
            common_1.CommonModule,
            router_1.RouterModule.forChild([
                { path: '', component: main_component_1.MainComponent, children: [
                        { path: '', redirectTo: 'home', pathMatch: 'full' },
                        { path: 'home', component: home_page_component_1.HomePageComponent, pathMatch: 'full' },
                        { path: 'detail', component: detail_component_1.DetailComponent, pathMatch: 'full' },
                        { path: 'thankyou', component: thankyou_component_1.ThankyouComponent, pathMatch: 'full' },
                        { path: 'camera', component: camera_component_1.CameraComponent, pathMatch: 'full' },
                        { path: '**', redirectTo: 'home', pathMatch: 'full' }
                    ]
                },
                { path: '**', redirectTo: '', pathMatch: 'full' }
            ])
        ],
        providers: [camera_service_1.CameraService],
        declarations: [home_page_component_1.HomePageComponent, detail_component_1.DetailComponent, main_component_1.MainComponent, camera_component_1.CameraComponent, thankyou_component_1.ThankyouComponent]
    })
], MainModule);
exports.MainModule = MainModule;


/***/ }),
/* 59 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

Object.defineProperty(exports, "__esModule", { value: true });
exports.environment = {
    env: 'dev',
    production: true,
    apiUrl: 'http://localhost:4001/',
    port: 4000
};


/***/ }),
/* 60 */
/***/ (function(module, exports) {

module.exports = require("@angular/common/http");

/***/ }),
/* 61 */
/***/ (function(module, exports) {

module.exports = require("rxjs/Subject");

/***/ }),
/* 62 */
/***/ (function(module, exports) {

module.exports = require("rxjs/add/operator/catch");

/***/ }),
/* 63 */
/***/ (function(module, exports) {

module.exports = require("rxjs/add/operator/map");

/***/ }),
/* 64 */
/***/ (function(module, exports) {

module.exports = require("rxjs/add/operator/share");

/***/ }),
/* 65 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(__dirname) {
Object.defineProperty(exports, "__esModule", { value: true });
__webpack_require__(45);
__webpack_require__(46);
var platform_server_1 = __webpack_require__(20);
var core_1 = __webpack_require__(0);
var app_server_module_ngfactory_1 = __webpack_require__(32);
var express = __webpack_require__(42);
var fs_1 = __webpack_require__(43);
var path_1 = __webpack_require__(44);
// let portEnv = process.env.npm_config_env
// var env;
// switch(portEnv){
// 	case 'prod' : 
// 		env = require('./environments/environment.prod');
// 	break;
// 	case 'dev' : 
// 		env = require('./environments/environment.dev');
// 	break;
// 	case 'staging' : 
// 		env = require('./environments/environment.staging');
// 	break;
// 	default: 
// 		env = require('./environments/environment.dev');
// 	break;
// }
var PORT = process.env.PORT || 4020;
core_1.enableProdMode();
var app = express();
var template = fs_1.readFileSync(path_1.join(__dirname, '..', 'dist', 'index.html')).toString();
app.engine('html', function (_, options, callback) {
    var opts = { document: template, url: options.req.url };
    platform_server_1.renderModuleFactory(app_server_module_ngfactory_1.AppServerModuleNgFactory, opts)
        .then(function (html) { return callback(null, html); });
});
app.set('view engine', 'html');
app.set('views', 'src');
app.get('*.*', express.static(path_1.join(__dirname, '..', 'dist')));
app.get('*', function (req, res) {
    res.render('index', { req: req });
});
app.listen(PORT, function () {
    console.log("listening on http://localhost:" + PORT + "!");
});

/* WEBPACK VAR INJECTION */}.call(exports, "src"))

/***/ })
/******/ ]);