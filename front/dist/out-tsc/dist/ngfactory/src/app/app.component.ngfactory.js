/**
 * @fileoverview This file is generated by the Angular template compiler.
 * Do not edit.
 * @suppress {suspiciousCode,uselessCode,missingProperties,missingOverride}
 */
/* tslint:disable */
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var i0 = require("./app.component.css.shim.ngstyle");
var i1 = require("@angular/core");
var i2 = require("@angular/router");
var i3 = require("../../node_modules/angular2-toaster/src/toaster-container.component.ngfactory");
var i4 = require("angular2-toaster/src/toaster-container.component");
var i5 = require("angular2-toaster/src/toaster.service");
var i6 = require("../../../../src/app/app.component");
var i7 = require("@angular/platform-browser");
var styles_AppComponent = [i0.styles];
exports.RenderType_AppComponent = i1.ɵcrt({ encapsulation: 0, styles: styles_AppComponent,
    data: {} });
function View_AppComponent_0(_l) {
    return i1.ɵvid(0, [(_l()(), i1.ɵeld(0, 16777216, null, null, 1, 'router-outlet', [], null, null, null, null, null)),
        i1.ɵdid(1, 212992, null, 0, i2.RouterOutlet, [i2.ChildrenOutletContexts,
            i1.ViewContainerRef, i1.ComponentFactoryResolver, [8, null], i1.ChangeDetectorRef], null, null), (_l()(), i1.ɵted(-1, null, ['\n'])), (_l()(),
            i1.ɵeld(3, 0, null, null, 1, 'toaster-container', [], null, null, null, i3.View_ToasterContainerComponent_0, i3.RenderType_ToasterContainerComponent)), i1.ɵdid(4, 245760, null, 0, i4.ToasterContainerComponent, [i5.ToasterService, i1.ChangeDetectorRef, i1.NgZone], { toasterconfig: [0, 'toasterconfig'] }, null), (_l()(), i1.ɵted(-1, null, ['\n']))], function (_ck, _v) {
        var _co = _v.component;
        _ck(_v, 1, 0);
        var currVal_0 = _co.toasterconfig;
        _ck(_v, 4, 0, currVal_0);
    }, null);
}
exports.View_AppComponent_0 = View_AppComponent_0;
function View_AppComponent_Host_0(_l) {
    return i1.ɵvid(0, [(_l()(), i1.ɵeld(0, 0, null, null, 1, 'app-root', [], null, null, null, View_AppComponent_0, exports.RenderType_AppComponent)),
        i1.ɵdid(1, 114688, null, 0, i6.AppComponent, [i2.Router, i7.Meta, i7.Title], null, null)], function (_ck, _v) {
        _ck(_v, 1, 0);
    }, null);
}
exports.View_AppComponent_Host_0 = View_AppComponent_Host_0;
exports.AppComponentNgFactory = i1.ɵccf('app-root', i6.AppComponent, View_AppComponent_Host_0, {}, {}, []);
//# sourceMappingURL=app.component.ngfactory.js.map