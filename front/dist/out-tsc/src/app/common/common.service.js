"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var angular2_cool_storage_1 = require("angular2-cool-storage");
var http_1 = require("@angular/http");
var CommonService = (function () {
    function CommonService(inj, localStorage, _http) {
        this.localStorage = localStorage;
        this._http = _http;
        this.authorised = false;
        this._apiUrl1 = 'http://localhost:4021/apis/';
        //super(inj);
        this._apiUrl = 'http://10.2.1.85/vinapp/';
    }
    CommonService.prototype.authorise = function () {
        if (this.localStorage.getItem("authToken")) {
            this.authorised = true;
        }
        else {
            this.authorised = false;
        }
        //console.log('common called after',this.authorised)
    };
    /****************************************************************************
    @PURPOSE      : User Login API
    @PARAMETERS   : <user_info_obj>
    @RETURN       : <success_data>
    ****************************************************************************/
    CommonService.prototype.uploadDisc = function (data) {
        var headers = new http_1.Headers({});
        var options = new http_1.RequestOptions({ headers: headers });
        //return this._http.post(this._apiUrl+'api/'+'userLogin',JSON.stringify(user),options)
        return this._http.post(this._apiUrl + 'upload_license.php', data, options)
            .map(function (response) { return response.json(); });
    };
    return CommonService;
}());
/****************************************************************************/
CommonService.decorators = [
    { type: core_1.Injectable },
];
/** @nocollapse */
CommonService.ctorParameters = function () { return [
    { type: core_1.Injector, },
    { type: angular2_cool_storage_1.CoolLocalStorage, },
    { type: http_1.Http, },
]; };
exports.CommonService = CommonService;
//# sourceMappingURL=common.service.js.map