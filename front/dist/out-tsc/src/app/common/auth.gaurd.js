"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var angular2_cool_storage_1 = require("angular2-cool-storage");
var CanLoginActivate = (function () {
    function CanLoginActivate(localStorage, router) {
        this.localStorage = localStorage;
        this.router = router;
    }
    CanLoginActivate.prototype.canActivate = function (route, state) {
        //console.log('can login activate called')
        if (!this.localStorage.getItem("authToken")) {
            return true;
        }
        this.router.navigate(['/main']);
        return false;
    };
    return CanLoginActivate;
}());
CanLoginActivate.decorators = [
    { type: core_1.Injectable },
];
/** @nocollapse */
CanLoginActivate.ctorParameters = function () { return [
    { type: angular2_cool_storage_1.CoolLocalStorage, },
    { type: router_1.Router, },
]; };
exports.CanLoginActivate = CanLoginActivate;
var AlwaysAuthChildrenGuard = (function () {
    function AlwaysAuthChildrenGuard(localStorage, router) {
        this.localStorage = localStorage;
        this.router = router;
    }
    AlwaysAuthChildrenGuard.prototype.canActivateChild = function (route, state) {
        //console.log('can child activate called')
        if (this.localStorage.getItem("authToken")) {
            return true;
        }
        this.router.navigate(['/login']);
        return false;
    };
    return AlwaysAuthChildrenGuard;
}());
AlwaysAuthChildrenGuard.decorators = [
    { type: core_1.Injectable },
];
/** @nocollapse */
AlwaysAuthChildrenGuard.ctorParameters = function () { return [
    { type: angular2_cool_storage_1.CoolLocalStorage, },
    { type: router_1.Router, },
]; };
exports.AlwaysAuthChildrenGuard = AlwaysAuthChildrenGuard;
//# sourceMappingURL=auth.gaurd.js.map