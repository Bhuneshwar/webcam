"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var core_2 = require("@angular/core");
var angular2_cool_storage_1 = require("angular2-cool-storage");
var angular2_toaster_1 = require("angular2-toaster");
var router_1 = require("@angular/router");
var ngx_cookie_service_1 = require("ngx-cookie-service");
var common_service_1 = require("./common.service");
var environment_1 = require("../../environments/environment");
var http_1 = require("@angular/http");
var BaseComponent = (function () {
    function BaseComponent(injector) {
        this.showBack = true;
        this.http = this.http;
        this.localStorage = injector.get(angular2_cool_storage_1.CoolLocalStorage); //Inject any service.
        this.toasterService = injector.get(angular2_toaster_1.ToasterService);
        this.router = injector.get(router_1.Router);
        this.cookie = injector.get(ngx_cookie_service_1.CookieService);
        this.commonService = injector.get(common_service_1.CommonService);
        this.http = injector.get(http_1.Http);
        this.router = injector.get(router_1.Router);
        //console.log('Your current Environment is :', environment)
        //this.popToast('success','test','message')
    }
    BaseComponent.prototype.checkBack = function () {
        if (location.pathname == '/main/home') {
            return false;
        }
        return true;
    };
    BaseComponent.prototype.goBack = function () {
        var stateName = this.router.url;
        if (stateName == '/main/detail') {
            this.router.navigate(['/main/home']);
        }
        else if (stateName == '/main/thankyou') {
            this.router.navigate(['/main/detail']);
        }
    };
    BaseComponent.prototype.setToken = function (key, value) {
        this.localStorage.setItem(key, value);
    };
    BaseComponent.prototype.getToken = function (key) {
        return this.localStorage.getItem(key);
    };
    BaseComponent.prototype.removeToken = function (key) {
        this.localStorage.removeItem(key);
    };
    BaseComponent.prototype.clearToken = function () {
        this.localStorage.clear();
    };
    BaseComponent.prototype.setCookie = function (key, value) {
        this.cookie.set(key, value);
    };
    BaseComponent.prototype.getCookie = function (key) {
        this.cookie.get(key);
    };
    BaseComponent.prototype.removeCookie = function (key) {
        this.cookie.delete(key);
    };
    BaseComponent.prototype.clearCookie = function () {
        this.cookie.deleteAll();
    };
    BaseComponent.prototype.popToast = function (type, title, body) {
        this.toasterService.pop(type, title, body);
    };
    //*************************************************************//
    //@Purpose : To prevent any key on keydown.
    //*************************************************************//
    BaseComponent.prototype.keyDownHandler = function (event, key) {
        if (key == 'space') {
            if (event.which === 32) {
                event.preventDefault();
            }
        }
    };
    BaseComponent.prototype.logout = function () {
        this.clearToken();
        //this.removeCookie('authToken')
        console.log('logout called '); //call logout api here.
        this.router.navigate(["/login"]);
    };
    /****************************************************************************
      @PURPOSE      : Test api call
      @PARAMETERS   : <user_info_obj>
      @RETURN       : <success_data>
    /****************************************************************************/
    BaseComponent.prototype.callApi = function () {
        // let headers = new Headers({'content-Type':'application/json'});
        // let options = new RequestOptions({headers : headers});
        return this.http.get(environment_1.environment.apiUrl + 'posts/1')
            .map(function (response) { return response.json(); });
    };
    return BaseComponent;
}());
/****************************************************************************/
BaseComponent.decorators = [
    { type: core_1.Component, args: [{
                selector: 'parent-comp',
                template: "<h1>Hello from ParentComponent </h1>",
                providers: []
            },] },
];
/** @nocollapse */
BaseComponent.ctorParameters = function () { return [
    { type: core_2.Injector, },
]; };
exports.BaseComponent = BaseComponent;
//# sourceMappingURL=commonComponent.js.map