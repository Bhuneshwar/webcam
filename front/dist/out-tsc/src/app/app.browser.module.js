"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var animations_1 = require("@angular/platform-browser/animations");
var app_module_1 = require("./app.module");
var app_component_1 = require("./app.component");
var AppBrowserModule = (function () {
    function AppBrowserModule() {
    }
    return AppBrowserModule;
}());
AppBrowserModule.decorators = [
    { type: core_1.NgModule, args: [{
                imports: [
                    animations_1.BrowserAnimationsModule,
                    app_module_1.AppModule
                ],
                bootstrap: [app_component_1.AppComponent]
            },] },
];
/** @nocollapse */
AppBrowserModule.ctorParameters = function () { return []; };
exports.AppBrowserModule = AppBrowserModule;
//# sourceMappingURL=app.browser.module.js.map