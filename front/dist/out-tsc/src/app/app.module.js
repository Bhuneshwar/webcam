"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var platform_browser_1 = require("@angular/platform-browser");
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var app_component_1 = require("./app.component");
exports.AppComponent = app_component_1.AppComponent;
var forms_1 = require("@angular/forms");
var angular2_cool_storage_1 = require("angular2-cool-storage");
var angular2_toaster_1 = require("angular2-toaster");
var commonComponent_1 = require("./common/commonComponent");
var auth_gaurd_1 = require("./common/auth.gaurd");
var ngx_cookie_service_1 = require("ngx-cookie-service");
var common_service_1 = require("./common/common.service");
var http_1 = require("@angular/http");
var AppModule = (function () {
    function AppModule() {
    }
    return AppModule;
}());
AppModule.decorators = [
    { type: core_1.NgModule, args: [{
                declarations: [
                    app_component_1.AppComponent,
                    commonComponent_1.BaseComponent
                ],
                imports: [
                    http_1.HttpModule,
                    angular2_cool_storage_1.CoolStorageModule, angular2_toaster_1.ToasterModule,
                    forms_1.FormsModule,
                    platform_browser_1.BrowserModule.withServerTransition({ appId: 'vin-frontend-ng4' }),
                    router_1.RouterModule.forRoot([
                        { path: 'main', loadChildren: './main/main.module#MainModule' },
                        { path: '', redirectTo: 'main', pathMatch: 'full' },
                        { path: '**', redirectTo: 'main', pathMatch: 'full' }
                    ], { initialNavigation: 'enabled', useHash: false })
                ],
                exports: [app_component_1.AppComponent],
                providers: [auth_gaurd_1.AlwaysAuthChildrenGuard, auth_gaurd_1.CanLoginActivate, ngx_cookie_service_1.CookieService, common_service_1.CommonService, angular2_toaster_1.ToasterService],
                bootstrap: [app_component_1.AppComponent]
            },] },
];
/** @nocollapse */
AppModule.ctorParameters = function () { return []; };
exports.AppModule = AppModule;
//# sourceMappingURL=app.module.js.map