"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
var Observable_1 = require("rxjs/Observable");
require("rxjs/add/operator/catch");
require("rxjs/add/operator/map");
var CameraService = (function () {
    function CameraService(http) {
        this.http = http;
        this.serviceUrl = 'http://localhost:61169/api/image';
    }
    CameraService.prototype.saveImage = function (imageData) {
        // console.log(imageData);
        var headers = new http_1.Headers({ 'Content-Type': 'application/json' });
        var options = new http_1.RequestOptions({ headers: headers, method: http_1.RequestMethod.Post });
        console.log('ska spara');
        // this.http.post(this.serviceUrl, { imageData }, options)
        //                 .map(this.extractData)
        //                 .catch(this.handleError);
        return this.http.post(this.serviceUrl, imageData, { headers: headers, method: http_1.RequestMethod.Post })
            .map(function (res) { return res.json(); })
            .catch(this.handleError);
    };
    CameraService.prototype.extractData = function (res) {
        var body = res.json();
        return body.data || {};
    };
    // private handleError (error: Response | any) {
    //   // In a real world app, we might use a remote logging infrastructure
    //   let errMsg: string;
    //   if (error instanceof Response) {
    //     const body = error.json() || '';
    //     const err = body.error || JSON.stringify(body);
    //     errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    //   } else {
    //     errMsg = error.message ? error.message : error.toString();
    //   }
    //   console.error(errMsg);
    //   return Observable.throw(errMsg);
    // }
    CameraService.prototype.handleError = function (error) {
        console.error('Error in the DepartureScheduleService');
        return Observable_1.Observable.throw(error.json().error || 'Server error');
    };
    return CameraService;
}());
CameraService.decorators = [
    { type: core_1.Injectable },
];
/** @nocollapse */
CameraService.ctorParameters = function () { return [
    { type: http_1.Http, },
]; };
exports.CameraService = CameraService;
//# sourceMappingURL=camera.service.js.map