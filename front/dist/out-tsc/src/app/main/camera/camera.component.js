"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var camera_service_1 = require("./camera.service");
var commonComponent_1 = require("../../common/commonComponent");
var CameraComponent = (function (_super) {
    __extends(CameraComponent, _super);
    function CameraComponent(injector, cameraService) {
        var _this = _super.call(this, injector) || this;
        _this.cameraService = cameraService;
        _this.showVideo = false; // Fungerar fast tvärtom. Byt logik
        return _this;
    }
    CameraComponent.prototype.capture = function () {
        // this.context = this.canvas.nativeElement.getContext('2d');
        this.context.drawImage(this.videoPlayer.nativeElement, 0, 0, this.width, this.height);
        this.showVideo = true;
    };
    CameraComponent.prototype.saveImage = function () {
        this.showVideo = false;
        var imgData = this.canvas.nativeElement.toDataURL('img/png');
        // console.log(imgData);
        this.myImg = imgData;
        imgData = imgData.replace('data:image/png;base64,', '');
        var postData = JSON.stringify({
            'ImageBase64String': imgData,
            'id': 3
        });
        console.log(postData);
        this.setToken('motor_img_url', this.myImg);
        this.popToast('success', 'Uploaded', 'Image Uploaded Successfully');
        //Call api here()
    };
    CameraComponent.prototype.ngOnInit = function () {
        this.width = 400;
        this.height = 300;
    };
    CameraComponent.prototype.ngAfterViewInit = function () {
        var _this = this;
        this.context = this.canvas.nativeElement.getContext('2d');
        if (navigator.mediaDevices && navigator.mediaDevices.getUserMedia) {
            setTimeout(function () {
                navigator.mediaDevices.getUserMedia({ video: true }).then(function (stream) {
                    if ("srcObject" in _this.videoPlayer.nativeElement) {
                        _this.videoPlayer.nativeElement.srcObject = stream;
                    }
                    else {
                        _this.videoPlayer.nativeElement.src = window.URL.createObjectURL(stream);
                    }
                    _this.videoPlayer.nativeElement.onloadedmetadata = function (e) {
                        if (this.videoPlayer && this.videoPlayer.nativeElement) {
                            this.videoPlayer.nativeElement.play();
                        }
                    };
                }).catch(function (err) {
                    alert('Error:' + err.message);
                });
            }, 2000);
        }
        else {
            alert("Doesn't support devices.");
        }
    };
    return CameraComponent;
}(commonComponent_1.BaseComponent));
CameraComponent.decorators = [
    { type: core_1.Component, args: [{
                selector: 'app-camera',
                templateUrl: './camera.component.html',
                styleUrls: ['./camera.component.css'],
                providers: [camera_service_1.CameraService]
            },] },
];
/** @nocollapse */
CameraComponent.ctorParameters = function () { return [
    { type: core_1.Injector, },
    { type: camera_service_1.CameraService, },
]; };
CameraComponent.propDecorators = {
    'videoPlayer': [{ type: core_1.ViewChild, args: ['videoplayer',] },],
    'canvas': [{ type: core_1.ViewChild, args: ['canvas',] },],
    'width': [{ type: core_1.Input },],
    'height': [{ type: core_1.Input },],
};
exports.CameraComponent = CameraComponent;
//# sourceMappingURL=camera.component.js.map