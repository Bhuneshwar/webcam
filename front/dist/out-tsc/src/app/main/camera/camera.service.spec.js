/* tslint:disable:no-unused-variable */
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var testing_1 = require("@angular/core/testing");
var camera_service_1 = require("./camera.service");
describe('CameraService', function () {
    beforeEach(function () {
        testing_1.TestBed.configureTestingModule({
            providers: [camera_service_1.CameraService]
        });
    });
    it('should ...', testing_1.inject([camera_service_1.CameraService], function (service) {
        expect(service).toBeTruthy();
    }));
});
//# sourceMappingURL=camera.service.spec.js.map