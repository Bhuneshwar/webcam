"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var core_2 = require("@angular/core");
var commonComponent_1 = require("../../common/commonComponent");
var ThankyouComponent = (function (_super) {
    __extends(ThankyouComponent, _super);
    function ThankyouComponent(injector) {
        return _super.call(this, injector) || this;
    }
    ThankyouComponent.prototype.ngOnInit = function () {
        //this.popToast('success','Welcome','Successfully logged in.')
    };
    return ThankyouComponent;
}(commonComponent_1.BaseComponent));
ThankyouComponent.decorators = [
    { type: core_1.Component, args: [{
                selector: 'app-thankyou',
                templateUrl: './thankyou.component.html'
            },] },
];
/** @nocollapse */
ThankyouComponent.ctorParameters = function () { return [
    { type: core_2.Injector, },
]; };
exports.ThankyouComponent = ThankyouComponent;
//# sourceMappingURL=thankyou.component.js.map