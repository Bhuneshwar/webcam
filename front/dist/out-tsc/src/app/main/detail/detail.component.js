"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var core_2 = require("@angular/core");
var commonComponent_1 = require("../../common/commonComponent");
var DetailComponent = (function (_super) {
    __extends(DetailComponent, _super);
    function DetailComponent(injector) {
        return _super.call(this, injector) || this;
    }
    DetailComponent.prototype.ngOnInit = function () {
        this.discData = JSON.parse(this.getToken('disc_data'));
        console.log('disc Data : ', this.discData);
        //this.popToast('success','Welcome','Successfully logged in.')
    };
    return DetailComponent;
}(commonComponent_1.BaseComponent));
DetailComponent.decorators = [
    { type: core_1.Component, args: [{
                selector: 'app-detail',
                templateUrl: './detail.component.html'
            },] },
];
/** @nocollapse */
DetailComponent.ctorParameters = function () { return [
    { type: core_2.Injector, },
]; };
exports.DetailComponent = DetailComponent;
//# sourceMappingURL=detail.component.js.map