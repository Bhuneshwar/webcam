"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var core_2 = require("@angular/core");
var commonComponent_1 = require("../../common/commonComponent");
var MainComponent = (function (_super) {
    __extends(MainComponent, _super);
    function MainComponent(injector) {
        var _this = _super.call(this, injector) || this;
        console.log('location', location);
        return _this;
    }
    MainComponent.prototype.ngOnInit = function () {
    };
    return MainComponent;
}(commonComponent_1.BaseComponent));
MainComponent.decorators = [
    { type: core_1.Component, args: [{
                selector: 'app-main',
                templateUrl: './main.component.html'
            },] },
];
/** @nocollapse */
MainComponent.ctorParameters = function () { return [
    { type: core_2.Injector, },
]; };
exports.MainComponent = MainComponent;
//# sourceMappingURL=main.component.js.map