"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var core_2 = require("@angular/core");
var commonComponent_1 = require("../../common/commonComponent");
var HomePageComponent = (function (_super) {
    __extends(HomePageComponent, _super);
    function HomePageComponent(injector) {
        return _super.call(this, injector) || this;
    }
    HomePageComponent.prototype.ngOnInit = function () {
        //this.popToast('success','Welcome','Successfully logged in.')
        this.showLoader = false;
        console.log('showLoader', this.showLoader);
    };
    HomePageComponent.prototype.onFileChange = function (event) {
        var _this = this;
        var reader = new FileReader();
        if (event.target.files && event.target.files.length > 0) {
            var file_1 = event.target.files[0];
            reader.readAsDataURL(file_1);
            reader.onload = function () {
                console.log('file', file_1);
                //Call file upload api here 
                //onsuccess redirect to next step
                var fd = new FormData();
                fd.append('image', file_1);
                _this.showLoader = true;
                _this.commonService.uploadDisc(fd).subscribe(function (success) {
                    _this.showLoader = false;
                    if (success.success == 1) {
                        var discData = {
                            licenseNo: success[6],
                            vehRegNo: success[7],
                            description: success[8],
                            make: success[9],
                            vin: success[12],
                            engineNo: success[13]
                        };
                        _this.setToken('disc_data', JSON.stringify(discData));
                        _this.router.navigate(['/main/detail']);
                    }
                    else if (success.success == 0) {
                        _this.popToast('error', 'Error', 'Please Scan the disc properly.');
                    }
                }, function (error) {
                    _this.showLoader = false;
                    console.log('error', error);
                    _this.popToast('error', 'Error', 'Please Scan the disc properly.');
                });
            };
        }
    };
    return HomePageComponent;
}(commonComponent_1.BaseComponent));
HomePageComponent.decorators = [
    { type: core_1.Component, args: [{
                selector: 'app-home-page',
                templateUrl: './home-page.component.html'
            },] },
];
/** @nocollapse */
HomePageComponent.ctorParameters = function () { return [
    { type: core_2.Injector, },
]; };
exports.HomePageComponent = HomePageComponent;
//# sourceMappingURL=home-page.component.js.map