"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var common_1 = require("@angular/common");
var home_page_component_1 = require("./home-page/home-page.component");
var main_component_1 = require("./main/main.component");
var detail_component_1 = require("./detail/detail.component");
var thankyou_component_1 = require("./thankyou/thankyou.component");
var camera_component_1 = require("./camera/camera.component");
var camera_service_1 = require("./camera/camera.service");
var MainModule = (function () {
    function MainModule() {
    }
    return MainModule;
}());
MainModule.decorators = [
    { type: core_1.NgModule, args: [{
                imports: [
                    common_1.CommonModule,
                    router_1.RouterModule.forChild([
                        { path: '', component: main_component_1.MainComponent, children: [
                                { path: '', redirectTo: 'home', pathMatch: 'full' },
                                { path: 'home', component: home_page_component_1.HomePageComponent, pathMatch: 'full' },
                                { path: 'detail', component: detail_component_1.DetailComponent, pathMatch: 'full' },
                                { path: 'thankyou', component: thankyou_component_1.ThankyouComponent, pathMatch: 'full' },
                                { path: 'camera', component: camera_component_1.CameraComponent, pathMatch: 'full' },
                                { path: '**', redirectTo: 'home', pathMatch: 'full' }
                            ]
                        },
                        { path: '**', redirectTo: '', pathMatch: 'full' }
                    ])
                ],
                providers: [camera_service_1.CameraService],
                declarations: [home_page_component_1.HomePageComponent, detail_component_1.DetailComponent, main_component_1.MainComponent, camera_component_1.CameraComponent, thankyou_component_1.ThankyouComponent]
            },] },
];
/** @nocollapse */
MainModule.ctorParameters = function () { return []; };
exports.MainModule = MainModule;
//# sourceMappingURL=main.module.js.map