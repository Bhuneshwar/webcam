"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var platform_browser_1 = require("@angular/platform-browser");
var angular2_toaster_1 = require("angular2-toaster");
var AppComponent = (function () {
    function AppComponent(_router, _meta, _title) {
        this._router = _router;
        this._meta = _meta;
        this._title = _title;
        this.toasterconfig = new angular2_toaster_1.ToasterConfig({
            showCloseButton: false,
            tapToDismiss: true,
            timeout: 3000,
            limit: 1
        });
        this.isLoggedIn = false;
    }
    AppComponent.prototype.ngOnInit = function () {
        var _this = this;
        this._router.events.subscribe(function (event) {
            if (event instanceof router_1.NavigationEnd) {
                switch (event.urlAfterRedirects) {
                    case '/':
                        _this._title.setTitle('Home Page');
                        _this._meta.updateTag({ name: 'description', content: 'Home Page Description' });
                        break;
                    case '/about':
                        _this._title.setTitle('About Page');
                        _this._meta.updateTag({ name: 'description', content: 'About Page Description' });
                        break;
                }
            }
        });
    };
    return AppComponent;
}());
AppComponent.decorators = [
    { type: core_1.Component, args: [{
                selector: 'app-root',
                templateUrl: './app.component.html',
                styleUrls: ['./app.component.css']
            },] },
];
/** @nocollapse */
AppComponent.ctorParameters = function () { return [
    { type: router_1.Router, },
    { type: platform_browser_1.Meta, },
    { type: platform_browser_1.Title, },
]; };
exports.AppComponent = AppComponent;
//# sourceMappingURL=app.component.js.map