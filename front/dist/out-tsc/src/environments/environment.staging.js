"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.environment = {
    env: 'staging',
    production: true,
    apiUrl: 'http://nodejs.test.com:4001/',
    port: 4000
};
//# sourceMappingURL=environment.staging.js.map