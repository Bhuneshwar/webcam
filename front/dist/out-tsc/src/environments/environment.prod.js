"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.environment = {
    env: 'prod',
    production: true,
    apiUrl: 'https://jsonplaceholder.typicode.com/',
    port: 8080
};
//# sourceMappingURL=environment.prod.js.map