"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.environment = {
    env: 'local',
    production: true,
    apiUrl: 'http://localhost:5001/',
    port: 5000
};
//# sourceMappingURL=environment.local.js.map